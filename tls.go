package wire

import (
	"context"
	"crypto/tls"
	"net"
	"sync/atomic"
	"time"
)

//*************************************************************************
// TLSHandshake Types
//*************************************************************************

const (
	// handshakeTTL defines time to set Read deadline for TLsHandshake.
	handshakeTTL = 2 * time.Second

	// maxTLSHandshakeTTLWait defines time to wait before we end connection because
	// tls handshake failed to happen in expected time.
	maxTLSHandshakeTTLWait = 7 * time.Second
)

// TLSHandshake defines a interface which handles the handshake
// process for processing a net.Conn connection to use tls.
type TLSHandshake interface {
	Upgrade(ctx context.Context, domain string, c net.Conn) (net.Conn, error)
}

// NoUpgrade implements TLSHandshake to provide no-upgrade behaviour, where
// the provided connections net.Conn are returned as is.
type NoUpgrade struct{}

// Upgrade returns the provided connection net.Conn.
func (NoUpgrade) Upgrade(ctx context.Context, domain string, c net.Conn) (net.Conn, error) {
	return c, nil
}

// SimpleClientTLSConfigUpgrader implements the TLSHandshake interface
// for handling server tls upgrade.
type SimpleClientTLSConfigUpgrader struct {
	Config *tls.Config
}

// Upgrade returns a connection which has being upgraded using tls.Client returning
// a tls connection.
func (stl SimpleClientTLSConfigUpgrader) Upgrade(ctx context.Context, domain string, c net.Conn) (net.Conn, error) {
	if stl.Config.ServerName == "" {
		stl.Config.ServerName = domain
	}

	return ClientConnToTLS(stl.Config, c)
}

// SimpleServerTLSConfigUpgrader implements the TLSHandshake interface
// for handling server tls upgrade.
type SimpleServerTLSConfigUpgrader struct {
	Config *tls.Config
}

// Upgrade returns a connection which has being upgraded using tls.Server returning
// a tls connection.
func (stl SimpleServerTLSConfigUpgrader) Upgrade(ctx context.Context, domain string, c net.Conn) (net.Conn, error) {
	return ServerConnToTLS(stl.Config, c)
}

// ServerConnToTLS transforms a giving net.Conn into a server tls connection.
func ServerConnToTLS(cfg *tls.Config, c net.Conn) (net.Conn, error) {
	cfg.BuildNameToCertificate()

	tlsConn := tls.Server(c, cfg)

	var handshaked int64

	// If we pass over this time and we have not being sorted then kill connection.
	time.AfterFunc(maxTLSHandshakeTTLWait, func() {
		if atomic.LoadInt64(&handshaked) != 1 {
			tlsConn.SetReadDeadline(time.Time{})
			tlsConn.Close()
		}
	})

	tlsConn.SetReadDeadline(time.Now().Add(handshakeTTL))
	if err := tlsConn.Handshake(); err != nil {
		atomic.StoreInt64(&handshaked, 1)
		tlsConn.Close()
		return nil, err
	}

	atomic.StoreInt64(&handshaked, 1)
	tlsConn.SetReadDeadline(time.Time{})

	return tlsConn, nil
}

// ClientConnToTLS transforms a giving net.Conn into a client tls connection.
func ClientConnToTLS(cfg *tls.Config, c net.Conn) (net.Conn, error) {
	cfg.BuildNameToCertificate()

	tlsConn := tls.Client(c, cfg)

	var handshaked int64

	// If we pass over this time and we have not being sorted then kill connection.
	time.AfterFunc(maxTLSHandshakeTTLWait, func() {
		if atomic.LoadInt64(&handshaked) != 1 {
			tlsConn.SetReadDeadline(time.Time{})
			tlsConn.Close()
		}
	})

	tlsConn.SetReadDeadline(time.Now().Add(handshakeTTL))
	if err := tlsConn.Handshake(); err != nil {
		atomic.StoreInt64(&handshaked, 1)
		tlsConn.Close()
		return nil, err
	}

	atomic.StoreInt64(&handshaked, 1)
	tlsConn.SetReadDeadline(time.Time{})

	return tlsConn, nil
}
