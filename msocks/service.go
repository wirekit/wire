package msocks

import (
	"context"
	"net"
	"sync"
	"time"

	"github.com/gokit/history"
	"github.com/wirekit/wire"
)

// WebsocketService embodies the composition of the wire.Waiter,
// wire.Clusters and wire.NetworkStats interfaces.
type WebsocketService interface {
	wire.Addr
	wire.Waiter
	wire.Clusters
	wire.NetworkStats
}

// ServeListener returns a WebsocketService to service tcp requests from provided listener.
func ServeListener(ctx context.Context, c Config, ll net.Listener) (WebsocketService, error) {
	service, err := Server(c)
	if err != nil {
		return nil, err
	}

	var srv webServiceImpl
	srv.ll = ll
	srv.ctx = ctx
	srv.srv = service
	srv.addr = ll.Addr()

	srv.waiter.Add(1)
	go srv.manage()

	return &srv, nil
}

type webServiceImpl struct {
	srv    *Network
	addr   net.Addr
	ll     net.Listener
	ctx    context.Context
	waiter sync.WaitGroup
}

// Close closes the underline service listener.
func (ts *webServiceImpl) Close() error {
	return ts.ll.Close()
}

// Addr returns the service net address.
func (ts *webServiceImpl) Addr() net.Addr {
	return ts.addr
}

// Wait blocks till giving service ends.
func (ts *webServiceImpl) Wait() {
	// Ensure that service has closed all internal logic.
	ts.srv.Wait()

	// Ensure that service method has also fully closed
	// goroutine.
	ts.waiter.Wait()
}

// Stats returns NetworkStatistics for the underline network operations.
func (ts *webServiceImpl) Stats() wire.NetworkStatistic {
	stats := ts.srv.Statistics()
	stats.LocalAddr = ts.addr
	stats.RemoteAddr = ts.addr
	return stats
}

// AddCluster adds giving address as a cluster into the underline network.
func (ts *webServiceImpl) AddCluster(addr string) error {
	return ts.srv.AddCluster(ts.addr, addr)
}

func (ts *webServiceImpl) manage() {
	defer ts.waiter.Done()

	addr := ts.ll.Addr()
	closer := ts.ctx.Done()

	initial := wire.MinTemporarySleep
	ctx := history.With("server-addr", ts.ll.Addr())

	go func() {
		<-ts.ctx.Done()
		ts.ll.Close()
	}()

	// listen for closure of swarm server and
	// end listener.
	for {
		conn, err := ts.ll.Accept()
		if err != nil {
			if ne, ok := err.(*net.OpError); ok {
				if ne.Temporary() {
					if initial >= wire.MaxTemporarySleep {
						return
					}

					time.Sleep(initial)
					initial *= 2
					continue
				}
			}

			ctx.Error(err, "connection closed through listener")
			ts.ll.Close()
			return
		}

		if initial >= wire.MaxTemporarySleep {
			initial = wire.MinTemporarySleep
		}

		ctx2 := ctx.WithFields(history.Attrs{
			"conn-local-addr":  conn.LocalAddr(),
			"conn-remote-addr": conn.RemoteAddr(),
		}).Info("new connection")

		if err := ts.srv.Handle(addr, conn, closer); err != nil {
			ctx2.Error(err, "failed to successfully handle connection")
			continue
		}
	}
}
