package msocks

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"errors"
	"net"
	"strings"
	"sync/atomic"
	"time"

	"context"

	"io"

	"github.com/gobwas/ws"
	"github.com/gobwas/ws/wsutil"
	"github.com/gokit/history"
	"github.com/influx6/faux/netutils"
	"github.com/wirekit/llio"
	"github.com/wirekit/wire"
	"github.com/wirekit/wire/internal"
)

var (
	wsReadBuffer  = 1024
	wsWriteBuffer = 1024
	wsClientState = ws.StateClientSide
)

// errors ...
var (
	ErrNoTLSConfig = errors.New("no tls.Config provided")
)

// ConnectOptions defines a function type used to apply given
// changes to a *clientNetwork type
type ConnectOptions func(conn *socketClient)

// MaxBuffer sets the clientNetwork to use the provided value
// as its maximum buffer size for it's writer.
func MaxBuffer(buffer int) ConnectOptions {
	return func(cm *socketClient) {
		cm.maxWrite = buffer
	}
}

// TLSConfig sets the giving tls.Config to be used by the returned
// client.
func TLSConfig(config *tls.Config) ConnectOptions {
	return func(cm *socketClient) {
		if config != nil {
			cm.secure = true
			cm.tls = config
		}
	}
}

// KeepAliveTimeout sets the client to use given timeout for it's connection net.Dialer
// keepAliveTimeout.
func KeepAliveTimeout(dur time.Duration) ConnectOptions {
	return func(cm *socketClient) {
		cm.keepTimeout = dur
	}
}

// Dialer sets the ws.Dialer to used creating a connection
// to the server.
func Dialer(dialer *ws.Dialer) ConnectOptions {
	return func(cm *socketClient) {
		cm.dialer = dialer
	}
}

// DialTimeout sets the client to use given timeout for it's connection net.Dialer
// dial timeout.
func DialTimeout(dur time.Duration) ConnectOptions {
	return func(cm *socketClient) {
		cm.dialTimeout = dur
	}
}

// NetworkID sets the id used by the client connection for identifying the
// associated network.
func NetworkID(id string) ConnectOptions {
	return func(cm *socketClient) {
		cm.nid = id
	}
}

// Connect is used to implement the client connection to connect to a
// mtcp.Network. It implements all the method functions required
// by the Client to communicate with the server. It understands
// the message length header sent along by every message and follows
// suite when sending to server.
func Connect(addr string, ops ...ConnectOptions) (wire.Client, error) {
	var c wire.Client
	c.ID = newID(40)

	addr = netutils.GetAddr(addr)
	host, _, _ := net.SplitHostPort(addr)

	network := new(socketClient)
	network.websocketServerClient = new(websocketServerClient)

	for _, op := range ops {
		op(network)
	}

	network.id = c.ID
	network.addr = addr
	network.hostname = host

	if network.nid == "" {
		network.nid = "no-network-id"
	}

	if network.network == "" {
		network.network = "udp"
	}

	if network.maxWrite <= 0 {
		network.maxWrite = wire.MaxBufferSize
	}

	if network.dialer == nil {
		network.dialer = &ws.Dialer{
			Timeout:         network.dialTimeout,
			ReadBufferSize:  wsReadBuffer,
			WriteBufferSize: wsWriteBuffer,
		}
	}

	network.logs = history.WithTags("websocket-client").With("id", network.id)
	network.parser = new(internal.TaggedMessages)

	c.NID = network.nid
	c.CloseFunc = network.close
	c.WriteToFunc = network.write
	c.WriteFunc = network.writeWithHeader
	c.FlushFunc = network.flush
	c.LiveFunc = network.isLive
	c.LiveFunc = network.isLive
	c.InfoFunc = network.getInfo
	c.ReaderFunc = network.clientRead
	c.BroadCastFunc = network.broadcast
	c.HasPendingFunc = network.hasPending
	c.StatisticFunc = network.getStatistics
	c.LocalAddrFunc = network.getLocalAddr
	c.RemoteAddrFunc = network.getRemoteAddr
	c.ReconnectionFunc = network.reconnect

	if err := network.reconnect(addr); err != nil {
		return c, err
	}

	return c, nil
}

type socketClient struct {
	*websocketServerClient
	addr        string
	hostname    string
	secure      bool
	tls         *tls.Config
	network     string
	dialer      *ws.Dialer
	keepTimeout time.Duration
	dialTimeout time.Duration
	started     int64
}

func (cn *socketClient) isStarted() bool {
	return atomic.LoadInt64(&cn.started) == 1
}

func (cn *socketClient) getInfo() wire.Info {
	addr := cn.addr
	if cn.remoteAddr != nil {
		addr = cn.remoteAddr.String()
	}

	return wire.Info{
		ID:         cn.id,
		ServerAddr: addr,
		MaxPayload: wire.MinBufferSize,
	}
}

func (cn *socketClient) sendHandshakeSkip() error {
	_, err := cn.writeWithHeader(handshakeSkipBytes)
	if err != nil {
		return err
	}

	return cn.flush()
}

func (cn *socketClient) broadcast(w io.WriterTo) (int64, error) {
	bu := guardPool.Get().(*bytes.Buffer)
	bu.Reset()
	defer guardPool.Put(bu)

	bu.Write(broadcastBytes)
	if n, err := w.WriteTo(bu); err != nil {
		return n, err
	}

	m, err := cn.writeSize(bu.Len(), bu)
	if err != nil {
		return m, err
	}

	return m, nil
}

func (cn *socketClient) handleCINFO() error {
	jsn, err := json.Marshal(cn.getInfo())
	if err != nil {
		return err
	}

	bu := guardPool.Get().(*bytes.Buffer)
	bu.Reset()

	defer guardPool.Put(bu)

	bu.Write(rinfoBytes)
	bu.Write(jsn)

	_, err = cn.writeSize(bu.Len(), bu)
	if err != nil {
		return err
	}

	return cn.flush()
}

func (cn *socketClient) close() error {
	if err := cn.isLive(); err != nil {
		return wire.ErrAlreadyClosed
	}

	err := cn.websocketServerClient.close()
	cn.waiter.Wait()
	return err
}

func (cn *socketClient) reconnect(addr string) error {
	if err := cn.isLive(); err == nil && cn.isStarted() {
		return nil
	}

	if !strings.HasPrefix(addr, "ws://") && !strings.HasPrefix(addr, "wss://") {
		addr = "ws://" + addr
	}

	if strings.HasPrefix(addr, "wss://") && cn.tls == nil {
		return ErrNoTLSConfig
	}

	defer atomic.StoreInt64(&cn.started, 1)

	cn.waiter.Wait()

	var conn net.Conn
	var err error

	if addr != "" {
		if conn, err = cn.getConn(addr); err != nil {
			conn, err = cn.getConn(cn.addr)
		}
	} else {
		conn, err = cn.getConn(cn.addr)
	}

	if err != nil {
		return err
	}

	cn.localAddr = conn.LocalAddr()
	cn.remoteAddr = conn.RemoteAddr()
	cn.serverAddr = conn.RemoteAddr()

	reader := wsutil.NewReader(conn, wsClientState)
	writer := wsutil.NewWriter(conn, wsClientState, ws.OpBinary)

	cn.cu.Lock()
	cn.conn = conn
	cn.cu.Unlock()

	cn.bu.Lock()
	cn.wsReader = reader
	cn.wsWriter = writer
	cn.bu.Unlock()

	// Have the handshake skipped on client.
	if err := cn.sendHandshakeSkip(); err != nil {
		return err
	}

	//Request server to send info with cluster information.
	if err := cn.sendCINFOReq(); err != nil {
		return err
	}

	//if err := cn.readInfo(conn, reader); err != nil {
	//	return err
	//}

	cn.waiter.Add(1)
	go cn.readLoop(conn, reader)

	return nil
}

func (cn *socketClient) readInfo(conn net.Conn, nx *wsutil.Reader) error {
	reader := llio.NewLengthRecvReader(nx)

	for {
		_, err := nx.NextFrame()
		if err != nil {
			cn.logs.Error(err, "read header error")

			// if its a timeout error-retry.
			if netErr, ok := err.(*net.OpError); ok && netErr.Timeout() {
				continue
			}

			return err
		}

		conn.SetReadDeadline(time.Now().Add(10 * time.Second))
		frame, err := reader.ReadHeader()
		if err != nil {
			cn.logs.Error(err, "read header error")
			conn.SetReadDeadline(time.Time{})

			// if its a timeout error-retry.
			if netErr, ok := err.(*net.OpError); ok && netErr.Timeout() {
				continue
			}

			return err
		}

		incoming := make([]byte, frame)
		_, err = reader.Read(incoming)
		if err != nil {
			cn.logs.Error(err, "read body error")
			conn.SetReadDeadline(time.Time{})

			// if its a timeout error-retry.
			if netErr, ok := err.(*net.OpError); ok && netErr.Timeout() {
				continue
			}

			return err
		}
		conn.SetReadDeadline(time.Time{})

		if bytes.Equal(incoming, cinfoBytes) {
			continue
		}

		if !bytes.HasPrefix(incoming, rinfoBytes) {
			return errors.New("expected rinfo response")
		}

		if err := cn.handleRINFO(bytes.TrimSpace(bytes.TrimPrefix(incoming, rinfoBytes))); err != nil {
			return err
		}

		break
	}

	return nil
}

// getConn returns net.Conn for giving addr.
func (cn *socketClient) getConn(addr string) (net.Conn, error) {
	bctx := cn.logs.WithFields(history.Attrs{
		"client-id": cn.id,
		"network":   cn.nid,
		"protocol":  "tcp",
	})

	var err error
	var conn net.Conn
	var hs ws.Handshake

	lastSleep := wire.MinTemporarySleep

	for {
		conn, _, hs, err = cn.dialer.Dial(context.Background(), addr)
		if err != nil {
			bctx.Error(err, "Failed to connect to server")

			if netErr, ok := err.(net.Error); ok && netErr.Temporary() {
				if lastSleep >= wire.MaxTemporarySleep {
					bctx.Red("Failed to connect to server with expired retries").With("err", err)
					return nil, err
				}

				time.Sleep(lastSleep)
				lastSleep *= 2
			}
			continue
		}
		break
	}

	cn.wshandshake = hs
	return conn, err
}
