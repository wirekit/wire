package msocks

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"io"
	"net"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"math/rand"

	"encoding/binary"

	"github.com/gobwas/ws"
	"github.com/gobwas/ws/wsutil"
	"github.com/gokit/history"
	"github.com/wirekit/llio"
	"github.com/wirekit/wire"
	"github.com/wirekit/wire/internal"
)

var (
	wsState                       = ws.StateServerSide
	cinfoBytes                    = []byte(wire.CINFO)
	rinfoBytes                    = []byte(wire.RINFO)
	clStatusBytes                 = []byte(wire.CLSTATUS)
	rescueBytes                   = []byte(wire.CRESCUE)
	handshakeCompletedBytes       = []byte(wire.CLHANDSHAKECOMPLETED)
	clientHandshakeCompletedBytes = []byte(wire.ClientHandShakeCompleted)
	handshakeSkipBytes            = []byte(wire.CLHandshakeSkip)
	broadcastBytes                = []byte(wire.BROADCAST)
)

var (
	guardPool = sync.Pool{New: func() interface{} {
		return new(bytes.Buffer)
	}}
)

//************************************************************************
//  Websocket Server Client Implementation
//************************************************************************

type websocketServerClient struct {
	maxWrite       int
	totalRead      int64
	totalWritten   int64
	totalFlushOut  int64
	totalWriteMsgs int64
	totalReadMsgs  int64
	id             string
	nid            string
	isACluster     bool
	config         Config
	logs           history.Ctx
	network        *Network
	localAddr      net.Addr
	remoteAddr     net.Addr
	serverAddr     net.Addr
	wshandshake    ws.Handshake
	parser         *internal.TaggedMessages
	closedCounter  int64
	waiter         sync.WaitGroup
	wsReader       *wsutil.Reader

	bu       sync.Mutex
	wsWriter *wsutil.Writer

	sim     sync.Mutex
	srcInfo *wire.Info
	cu      sync.Mutex
	conn    net.Conn
}

func (sc *websocketServerClient) isLive() error {
	if atomic.LoadInt64(&sc.closedCounter) == 1 {
		return wire.ErrAlreadyClosed
	}

	sc.bu.Lock()
	defer sc.bu.Unlock()

	if sc.wsWriter == nil {
		return wire.ErrAlreadyClosed
	}

	return nil
}

// isCluster returns true/false if a giving connection is actually
// connected to another server i.e a server-to-server and not a
// client-to-server connection.
func (sc *websocketServerClient) isCluster() bool {
	return sc.isACluster
}

func (sc *websocketServerClient) getRemoteAddr() (net.Addr, error) {
	return sc.remoteAddr, nil
}

func (sc *websocketServerClient) getLocalAddr() (net.Addr, error) {
	return sc.localAddr, nil
}

func (sc *websocketServerClient) getStatistics() (wire.ClientStatistic, error) {
	var stats wire.ClientStatistic
	stats.ID = sc.id
	stats.Local = sc.localAddr
	stats.Remote = sc.remoteAddr
	stats.BytesRead = atomic.LoadInt64(&sc.totalRead)
	stats.BytesFlushed = atomic.LoadInt64(&sc.totalFlushOut)
	stats.BytesWritten = atomic.LoadInt64(&sc.totalWritten)
	stats.MessagesRead = atomic.LoadInt64(&sc.totalReadMsgs)
	stats.MessagesWritten = atomic.LoadInt64(&sc.totalWriteMsgs)
	return stats, nil
}

func (sc *websocketServerClient) write(w io.WriterTo) (int64, error) {
	if err := sc.isLive(); err != nil {
		return 0, err
	}

	bu := guardPool.Get().(*bytes.Buffer)
	bu.Reset()
	defer guardPool.Put(bu)

	n, err := w.WriteTo(bu)
	if err != nil {
		return n, err
	}

	if int(n) >= sc.config.MaxPayload {
		return n, wire.ErrToBig
	}

	return sc.writeSize(bu.Len(), bu)
}

func (sc *websocketServerClient) writeSize(size int, bu io.WriterTo) (int64, error) {
	var inco [4]byte
	area := inco[0:4]

	sc.bu.Lock()
	defer sc.bu.Unlock()

	if sc.wsWriter == nil {
		return 0, wire.ErrAlreadyClosed
	}

	var shouldFlush bool
	buffered := sc.wsWriter.Buffered()
	available := sc.wsWriter.Available()

	if buffered+4+size > available {
		shouldFlush = true
	}

	binary.BigEndian.PutUint32(area, uint32(size))
	sc.wsWriter.Write(area)

	n, err := bu.WriteTo(sc.wsWriter)
	if err != nil {
		return n, err
	}

	if int(n) != size {
		return n, io.ErrShortWrite
	}

	if shouldFlush {
		err = sc.wsWriter.Flush()
	}

	return n, nil
}

func (sc *websocketServerClient) writeWithHeader(d []byte) (int, error) {
	sc.bu.Lock()
	defer sc.bu.Unlock()

	if sc.wsWriter == nil {
		return 0, wire.ErrAlreadyClosed
	}

	size := len(d)

	var shouldFlush bool
	buffered := sc.wsWriter.Buffered()
	available := sc.wsWriter.Available()

	if buffered+4+size > available {
		shouldFlush = true
	}

	var inco [4]byte
	area := inco[0:4]

	binary.BigEndian.PutUint32(area, uint32(size))
	sc.wsWriter.Write(area)
	n, err := sc.wsWriter.Write(d)
	if err != nil {
		return n, err
	}

	if shouldFlush {
		err = sc.wsWriter.Flush()
	}

	return n, err
}

func (sc *websocketServerClient) broadcast(w io.WriterTo) (int64, error) {
	if err := sc.isLive(); err != nil {
		return 0, err
	}

	bu := guardPool.Get().(*bytes.Buffer)
	bu.Reset()
	defer guardPool.Put(bu)

	n, err := w.WriteTo(bu)
	if err != nil {
		return n, err
	}

	if int(n) >= sc.config.MaxPayload {
		return n, wire.ErrToBig
	}

	sc.network.cu.Lock()
	defer sc.network.cu.Unlock()

	atomic.AddInt64(&sc.totalReadMsgs, 1)
	atomic.AddInt64(&sc.totalWritten, int64(bu.Len()))

	for _, client := range sc.network.clients {
		if _, err := client.writeWithHeader(bu.Bytes()); err != nil {
			sc.logs.WithFields(history.Attrs{
				"from-client": sc.id,
				"to-client":   client.id,
				"data":        string(bu.Bytes()),
			}).Error(err, "failed to broadcast data")
		}
	}

	return n, nil
}

func (sc *websocketServerClient) writeDirectly(d ...[]byte) error {
	if len(d) == 0 {
		return nil
	}

	if err := sc.isLive(); err != nil {
		return err
	}

	var conn net.Conn
	sc.cu.Lock()
	if sc.conn == nil {
		sc.cu.Unlock()
		return wire.ErrAlreadyClosed
	}
	conn = sc.conn
	sc.cu.Unlock()

	sc.bu.Lock()
	defer sc.bu.Unlock()

	if sc.wsWriter == nil {
		return wire.ErrAlreadyClosed
	}

	for _, item := range d {
		available := sc.wsWriter.Available()

		// if what is available is less than what we have
		// available, then flush first.
		toWrite := len(item) + wire.HeaderLength
		if available < toWrite {
			buffered := sc.wsWriter.Buffered()
			atomic.AddInt64(&sc.totalFlushOut, int64(buffered))

			conn.SetWriteDeadline(time.Now().Add(wire.MaxFlushDeadline))
			if err := sc.wsWriter.Flush(); err != nil {
				conn.SetWriteDeadline(time.Time{})
				return err
			}
			conn.SetWriteDeadline(time.Time{})
		}

		if _, err := sc.wsWriter.Write(item); err != nil {
			return err
		}
	}

	return nil
}

func (sc *websocketServerClient) distribute(skipSelf bool, skipCluster bool, flush bool, data []byte) error {
	sc.network.cu.Lock()
	defer sc.network.cu.Unlock()

	var err error
	for _, client := range sc.network.clients {
		// If skipself is true, then skip this instance from
		// write.
		if skipSelf && (client.id == sc.id || client == sc) {
			continue
		}

		// If skipCluster is true and we are a cluster, then skip this
		// instance from write. We do this because attempting to wire
		// to all cluster here, will calls a endless propagation, has
		// other clusters will be sending back to the source. Its important
		// to only ever ensure if a cluster is sent to then it's done by the
		// source of the message, allowing other clusters to just distribute
		// to their internal network client connections.
		if skipCluster && client.isCluster() {
			continue
		}

		_, err = client.writeWithHeader(data)
		if err != nil {
			sc.logs.WithFields(history.Attrs{
				"from-client": sc.id,
				"to-client":   client.id,
				"data":        string(data),
			}).Error(err, "failed to broadcast data")
		}

		// If flush is set, then flush all existing data
		// into connection.
		if flush {
			if err := client.flush(); err != nil {
				sc.logs.WithFields(history.Attrs{
					"from-client": sc.id,
					"to-client":   client.id,
					"data":        string(data),
				}).Error(err, "failed to flush broadcasted data")
			}
		}
	}

	return nil
}

func (sc *websocketServerClient) clientRead() ([]byte, error) {
	if err := sc.isLive(); err != nil {
		return nil, err
	}

	return sc.clientNextData()
}

func (sc *websocketServerClient) clientNextData() ([]byte, error) {
	indata, err := sc.parser.Next()
	atomic.AddInt64(&sc.totalRead, int64(len(indata)))
	if err != nil {
		return nil, err
	}

	atomic.AddInt64(&sc.totalReadMsgs, 1)

	// if its a broadcast, just trim the header out.
	if bytes.HasPrefix(indata, broadcastBytes) {
		return bytes.TrimPrefix(indata, broadcastBytes), nil
	}

	if bytes.Equal(indata, rescueBytes) {
		return nil, wire.ErrNoDataYet
	}

	if bytes.HasPrefix(indata, rinfoBytes) {
		if err := sc.handleRINFO(rinfoBytes); err != nil {
			return nil, err
		}
		return nil, wire.ErrNoDataYet
	}

	if bytes.HasPrefix(indata, cinfoBytes) {
		if err := sc.handleCINFO(); err != nil {
			return nil, err
		}
		return nil, wire.ErrNoDataYet
	}

	return indata, nil
}

func (sc *websocketServerClient) serverRead() ([]byte, error) {
	if err := sc.isLive(); err != nil {
		if data, cerr := sc.serverNextData(); cerr == nil {
			return data, err
		}
		return nil, err
	}

	return sc.serverNextData()
}

func (sc *websocketServerClient) serverNextData() ([]byte, error) {
	indata, err := sc.parser.Next()
	if err != nil {
		return nil, err
	}

	atomic.AddInt64(&sc.totalReadMsgs, 1)
	atomic.AddInt64(&sc.totalRead, int64(len(indata)))

	// We are on the server, if we are not a cluster connection
	// then return to reader else distribute to our own non-cluster
	// connections.
	if bytes.HasPrefix(indata, broadcastBytes) {
		if !sc.isCluster() {
			// Send to all network clients including clusters.
			sc.distribute(true, false, true, indata)

			return bytes.TrimPrefix(indata, broadcastBytes), nil
		}

		// Send to all network clients excluding clusters.
		sc.distribute(true, true, true, indata)

		return nil, wire.ErrNoDataYet
	}

	if bytes.HasPrefix(indata, clientHandshakeCompletedBytes) {
		return nil, wire.ErrNoDataYet
	}

	if bytes.HasPrefix(indata, cinfoBytes) {
		if err := sc.handleCINFO(); err != nil {
			sc.logs.WithTags("websocket-server").With("client", sc.id).Error(err, "failed to pre-flush data")
			return nil, err
		}
		return nil, wire.ErrNoDataYet
	}

	if bytes.HasPrefix(indata, clStatusBytes) {
		if err := sc.handleCLStatusReceive(indata); err != nil {
			history.WithTags("websocket-server").With("client", sc.id).Error(err, "failed to receive correct status response")
			return nil, err
		}
		return nil, wire.ErrNoDataYet
	}

	return indata, nil
}

func (sc *websocketServerClient) hasPending() bool {
	if err := sc.isLive(); err != nil {
		return false
	}

	sc.bu.Lock()
	defer sc.bu.Unlock()
	if sc.wsWriter == nil {
		return false
	}

	return sc.wsWriter.Buffered() > 0
}

func (sc *websocketServerClient) buffered() (float64, error) {
	if err := sc.isLive(); err != nil {
		return 0, err
	}

	sc.bu.Lock()
	defer sc.bu.Unlock()

	available := float64(sc.wsWriter.Available())
	buffered := float64(sc.wsWriter.Buffered())
	return buffered / available, nil
}

func (sc *websocketServerClient) flush() error {
	if err := sc.isLive(); err != nil {
		return err
	}

	var conn net.Conn
	sc.cu.Lock()
	conn = sc.conn
	sc.cu.Unlock()

	if conn == nil {
		return wire.ErrAlreadyClosed
	}

	sc.bu.Lock()
	defer sc.bu.Unlock()

	if sc.wsWriter == nil {
		return wire.ErrAlreadyClosed
	}

	if sc.wsWriter.Buffered() != 0 {
		conn.SetWriteDeadline(time.Now().Add(wire.MaxFlushDeadline))
		if err := sc.wsWriter.Flush(); err != nil {
			conn.SetWriteDeadline(time.Time{})
			sc.logs.WithTitle("websocketServerClient.flush").
				Error(err, "failed to receive status request")
			return err
		}
		conn.SetWriteDeadline(time.Time{})
	}

	return nil
}

func (sc *websocketServerClient) close() error {
	logs := sc.logs.WithTitle("websocketServerClient.close")
	if err := sc.isLive(); err != nil {
		logs.Error(err, "closing websocket node connection")
		return err
	}

	logs.Info("closing websocket node connection")

	atomic.StoreInt64(&sc.closedCounter, 1)

	sc.flush()

	var err error
	sc.cu.Lock()
	if sc.conn != nil {
		err = sc.conn.Close()
	}
	sc.cu.Unlock()

	sc.waiter.Wait()

	sc.bu.Lock()
	sc.wsWriter = nil
	sc.wsReader = nil
	sc.bu.Unlock()

	sc.cu.Lock()
	sc.conn = nil
	sc.cu.Unlock()

	if err != nil {
		logs.Error(err, "closing websocket node connection")
	}

	return err
}

// readLoop handles the necessary operation of reading data from the
// underline connection.
func (sc *websocketServerClient) readLoop(conn net.Conn, reader *wsutil.Reader) {
	defer sc.close()
	defer sc.waiter.Done()

	lreader := llio.NewLengthRecvReader(reader)
	incoming := make([]byte, wire.SmallestMinBufferSize)

	logs := sc.logs.WithTitle("websocketServerClient.readLoop")

	for {

		wsFrame, err := reader.NextFrame()
		if err != nil {
			logs.With("remoteAddr", conn.RemoteAddr()).
				Error(err, "failed to read websocket data frame")
			return
		}

		frame, err := lreader.ReadHeader()
		if err != nil {
			logs.With("remoteAddr", conn.RemoteAddr()).
				Error(err, "failed to read data header")
			return
		}

		incoming = make([]byte, frame)

		n, err := lreader.Read(incoming)
		if err != nil {
			logs.With("remoteAddr", conn.RemoteAddr()).
				Error(err, "failed to read data header")
			return
		}

		datalogs := logs.WithFields(history.Attrs{
			"length":     n,
			"ws-frame":   wsFrame,
			"data":       string(incoming),
			"remoteAddr": conn.RemoteAddr(),
		}).Info("received websocket message")

		atomic.AddInt64(&sc.totalRead, int64(len(incoming[:n])))

		// Send into go-routine (critical path)?
		if err := sc.parser.Parse(incoming[:n]); err != nil {
			datalogs.Error(err, "failed to parse data")
			return
		}
	}
}

func (sc *websocketServerClient) getInfo() wire.Info {
	var srcInfo *wire.Info

	sc.sim.Lock()
	srcInfo = sc.srcInfo
	sc.sim.Unlock()

	var base wire.Info
	if sc.isCluster() && srcInfo != nil {
		base = *srcInfo
	} else {
		var meta wire.Meta
		var serverNode bool

		if sc.network != nil {
			meta = sc.network.config.Meta
			serverNode = true
		}

		base = wire.Info{
			ID:         sc.id,
			ServerNode: serverNode,
			Meta:       meta,
			Cluster:    sc.isACluster,
			MaxPayload: int64(sc.maxWrite),
			ServerAddr: sc.serverAddr.String(),
		}
	}

	if sc.network != nil {
		if others, err := sc.network.getAllClient(sc.id); err != nil {
			for _, other := range others {
				if !other.IsCluster() {
					continue
				}
				base.ClusterNodes = append(base.ClusterNodes, other.Info())
			}
		}
	}

	return base
}

func (sc *websocketServerClient) handleCLStatusReceive(data []byte) error {
	data = bytes.TrimPrefix(data, clStatusBytes)

	var info wire.Info
	if err := json.Unmarshal(data, &info); err != nil {
		return err
	}

	sc.srcInfo = &info
	sc.network.registerCluster(sc.serverAddr, info.ClusterNodes)

	_, err := sc.writeWithHeader(handshakeCompletedBytes)
	if err != nil {
		return err
	}

	return sc.flush()
}

func (sc *websocketServerClient) handleCLStatusSend() error {
	var info wire.Info
	info.ID = sc.nid
	info.Cluster = true
	info.ServerNode = true
	info.Meta = sc.network.config.Meta
	info.MaxPayload = int64(sc.maxWrite)
	info.ServerAddr = sc.serverAddr.String()

	if others, err := sc.network.getAllClient(sc.id); err != nil {
		for _, other := range others {
			if !other.IsCluster() {
				continue
			}

			info.ClusterNodes = append(info.ClusterNodes, other.Info())
		}
	}

	jsn, err := json.Marshal(info)
	if err != nil {
		return err
	}

	bu := guardPool.Get().(*bytes.Buffer)
	bu.Reset()
	defer guardPool.Put(bu)

	bu.Write(clStatusBytes)
	bu.Write(jsn)

	_, err = sc.writeSize(bu.Len(), bu)
	if err != nil {
		return err
	}

	return sc.flush()
}

func (sc *websocketServerClient) handleCINFO() error {
	jsn, err := json.Marshal(sc.getInfo())
	if err != nil {
		return err
	}

	bu := guardPool.Get().(*bytes.Buffer)
	bu.Reset()
	defer guardPool.Put(bu)

	bu.Write(rinfoBytes)
	bu.Write(jsn)

	_, err = sc.writeSize(bu.Len(), bu)
	if err != nil {
		return err
	}

	return sc.flush()
}

func (sc *websocketServerClient) handleRINFO(data []byte) error {
	rdata := bytes.TrimPrefix(data, rinfoBytes)
	if string(rdata) == "" {
		sc.logs.With("data", string(data)).Yellow("empty rinfo")
		return nil
	}

	var info wire.Info
	if err := json.Unmarshal(rdata, &info); err != nil {
		sc.logs.With("data", string(data)).Error(err, "failed to unmarshal")
		return err
	}

	info.Cluster = sc.isACluster

	sc.sim.Lock()
	sc.srcInfo = &info
	sc.sim.Unlock()

	if sc.network != nil {
		sc.network.registerCluster(sc.serverAddr, info.ClusterNodes)
	}

	return nil
}

func (sc *websocketServerClient) sendCLHSCompleted() error {
	if _, err := sc.writeWithHeader(clientHandshakeCompletedBytes); err != nil {
		return err
	}

	if err := sc.flush(); err != nil {
		return err
	}

	return nil
}

func (sc *websocketServerClient) sendCINFOReq() error {
	if _, err := sc.writeWithHeader(cinfoBytes); err != nil {
		return err
	}

	if err := sc.flush(); err != nil {
		return err
	}

	return nil
}

func (sc *websocketServerClient) handshake() error {
	logs := sc.logs.WithTitle("websocketServerClient.handshake")

	if err := sc.sendCINFOReq(); err != nil {
		logs.Error(err, "error sending CINFO request")
		logs.Red("Failed to send CINFO request")
		return err
	}

	before := time.Now()

	// Wait for RCINFO response from connection.
	for {
		msg, err := sc.serverRead()
		if err != nil {
			if err != wire.ErrNoDataYet {
				return err
			}

			if time.Now().Sub(before) > wire.MaxInfoWait {
				sc.close()
				logs.Error(wire.ErrFailedToRecieveInfo, "read timeout")
				logs.Red("read timeout")
				return wire.ErrFailedToRecieveInfo
			}
			continue
		}

		if bytes.Equal(msg, handshakeSkipBytes) && !sc.isACluster {
			sc.logs.Info("handshake process skipped")
			return nil
		}

		// if we get a rescue signal, then client never got our CINFO request, so resent.
		if bytes.Equal(msg, rescueBytes) {
			logs.Info("received rescue request")

			if err := sc.sendCINFOReq(); err != nil {
				logs.Error(err, "failed to send CINFO after rescue request")
				logs.Red("send error with CINFO after rescue")
				return err
			}

			// reset time used for tracking timeout.
			before = time.Now()
			continue
		}

		// First message should be a wire.RCINFO response.
		if !bytes.HasPrefix(msg, rinfoBytes) {
			sc.close()

			logs.Error(wire.ErrFailedToRecieveInfo, "failed to receive RCINFO response")
			logs.Red("failed handshake at RCINFO response")
			return wire.ErrFailedToRecieveInfo
		}

		if err := sc.handleRINFO(msg); err != nil {
			logs.Error(err, "failed to process RCINFO response")
			logs.Red("failed handshake at RCINFO response process")
			return err
		}

		break
	}

	// if its a cluster send Cluster Status message.
	if sc.isACluster {
		logs.Info("client cluster handshake agreement processing")

		if err := sc.handleCLStatusSend(); err != nil {
			logs.Error(err, "failed to send CLStatus message")
			logs.Red("failed handshake at CLStatus message")
			return err
		}

		before = time.Now()

		// Wait for handshake completion signal.
		for {
			msg, err := sc.serverRead()
			if err != nil {
				if err != wire.ErrNoDataYet {
					return err
				}

				if time.Now().Sub(before) > wire.MaxInfoWait {
					sc.close()
					logs.Error(wire.ErrFailedToCompleteHandshake, "response timeout")
					logs.Red("failed to complete handshake")
					return wire.ErrFailedToCompleteHandshake
				}
				continue
			}

			// if we get a rescue signal, then client never got our CLStatus response, so resend.
			if bytes.Equal(msg, rescueBytes) {
				if err := sc.handleCLStatusSend(); err != nil {
					logs.Error(err, "failed to send CLStatus response message")
					logs.Red("failed to complete handshake at CLStatus")
					return err
				}

				// reset time used for tracking timeout.
				before = time.Now()
				continue
			}

			if !bytes.Equal(msg, handshakeCompletedBytes) {
				logs.Error(wire.ErrFailedToCompleteHandshake, "failed to received handshake completion")
				logs.Red("failed to complete handshake at completion signal")
				return wire.ErrFailedToCompleteHandshake
			}

			break
		}
	} else {
		if err := sc.sendCLHSCompleted(); err != nil {
			logs.Error(err, "failed to deliver CLHS handshake completion signal")
			logs.Red("failed non-cluster handshake completion")
			return err
		}
	}

	logs.Info("handshake completed")

	return nil
}

//************************************************************************
//  Websocket Server Implementation
//************************************************************************

// Config provides properties for initiating tcp network.
type Config struct {
	// ServerName sets the desired name to be used for the server.
	ServerName string

	// Handshake provides implementation for processing
	// connections tls handshake.
	Handshake wire.TLSHandshake

	// Handler provides the handler to be called to service clients.
	Handler wire.ConnHandler

	// Hook provides a means to get hook into the lifecycle-processes of
	// the network and client connection and disconnection.
	Hook wire.Hook

	// Upgrader is used to upgrade normal tcp net.Conns into websocket
	// connection.
	Upgrader *ws.Upgrader

	// Dialer to be used to create net.Conn to connect to cluster address
	// in TCPNetwork.AddCluster. Set to use a custom dialer.
	Dialer *ws.Dialer

	// Meta contains user defined gacts for this server which will be send along
	// the transfer. Always ensure not to keep large objects or info in here. It
	// is expected to be small.
	Meta wire.Meta

	// MaxClusterRetries specifies allowed max retries used by the
	// RetryPolicy for reconnecting to a disconnected cluster network.
	MaxClusterRetries int

	// ClusterRetryDelay defines the duration used to expand for
	// each retry of reconnecting to a cluster address.
	ClusterRetryDelay time.Duration

	// MaxConnection defines the total allowed connections for
	// giving network.
	MaxConnections int

	// MaxInfoWait defines the max time a connection awaits the completion of a
	// handshake phase.
	MaxInfoWait time.Duration

	// MaxBufferSize sets given max size of buffer for client, each client writes collected
	// till flush must not exceed else will not be buffered and will be written directly.
	MaxBufferSize int

	// MaxPayload sets given maximum value of payload size the server will accept
	// else closing connection.
	MaxPayload int
}

func (c *Config) init() error {
	if c.Handler == nil {
		return errors.New("Config.Handler is required")
	}

	if c.Handshake == nil {
		c.Handshake = wire.NoUpgrade{}
	}

	if c.Upgrader == nil {
		c.Upgrader = &ws.Upgrader{}
	}

	if c.Dialer == nil {
		c.Dialer = &ws.Dialer{
			ReadBufferSize:  wsReadBuffer,
			WriteBufferSize: wsWriteBuffer,
			Timeout:         wire.DefaultDialTimeout,
		}
	}

	if c.MaxBufferSize <= 0 {
		c.MaxBufferSize = wire.MaxBufferSize
	}

	if c.MaxPayload <= 0 {
		c.MaxPayload = wire.MaxPayloadSize
	}

	if c.MaxConnections <= 0 {
		c.MaxConnections = wire.MaxConnections
	}

	if c.MaxClusterRetries <= 0 {
		c.MaxClusterRetries = wire.MaxReconnectRetries
	}

	if c.ClusterRetryDelay <= 0 {
		c.ClusterRetryDelay = wire.DefaultClusterRetryDelay
	}

	return nil
}

// Network defines a network which runs ontop of provided wire.ConnHandler.
type Network struct {
	id           string
	totalClients int64
	totalClosed  int64
	totalActive  int64
	totalOpened  int64
	config       Config
	cu           sync.RWMutex
	clients      map[string]*websocketServerClient
	routines     sync.WaitGroup
}

// Server returns a new instance of a Network.
func Server(c Config) (*Network, error) {
	if err := c.init(); err != nil {
		return nil, err
	}

	return &Network{
		config:  c,
		id:      newID(40),
		clients: make(map[string]*websocketServerClient),
	}, nil
}

// Handle handles the provided connection and instantiates it as necessary.
func (n *Network) Handle(addr net.Addr, conn net.Conn, c <-chan struct{}) error {
	serverName := n.config.ServerName
	if serverName == "" {
		serverName, _, _ = net.SplitHostPort(addr.String())
	}

	conn, err := n.config.Handshake.Upgrade(context.Background(), serverName, conn)
	if err != nil {
		return err
	}

	// setup a goroutine to listen for request to close connection.
	go func() {
		<-c
		conn.Close()
	}()

	// if we have maximum allowed connections, then close new conn.
	if int(atomic.LoadInt64(&n.totalOpened)) >= n.config.MaxConnections {
		return conn.Close()
	}

	atomic.AddInt64(&n.totalClients, 1)
	atomic.AddInt64(&n.totalOpened, 1)
	return n.addClient(addr, conn, nil, false)
}

func (n *Network) registerCluster(serverAddr net.Addr, clusters []wire.Info) {
	for _, cluster := range clusters {
		if err := n.AddCluster(serverAddr, cluster.ServerAddr); err != nil {
			history.With("info", cluster).
				Error(err, "failed to add cluster address")
		}
	}
}

// AddCluster attempts to add new connection to another mnet tcp server.
// It will attempt to dial specified address returning an error if the
// giving address failed or was not able to meet the handshake protocols.
func (n *Network) AddCluster(serverAddr net.Addr, addr string) error {
	log := history.WithTitle("Network.AddCluster").With("addr", addr)

	if n.connectedToMeByAddr(addr) {
		log.Error(wire.ErrAlreadyServiced, "cluster already exists")
		log.Red("failed to cluster with network")
		return wire.ErrAlreadyServiced
	}

	if strings.HasPrefix(addr, "wss://") {
		addr = "ws://" + strings.TrimPrefix(addr, "wss://")
	}

	if !strings.HasPrefix(addr, "ws://") {
		addr = "ws://" + addr
	}

	conn, _, hs, err := n.config.Dialer.Dial(context.Background(), addr)
	if err != nil {
		log.Error(err, "unable to dial network address")
		log.Red("failed to cluster with network")
		return err
	}

	if n.connectedToMeByAddr(conn.RemoteAddr().String()) {
		conn.Close()
		log.Error(wire.ErrAlreadyServiced, "cluster address exists already")
		log.Red("failed to cluster with network")
		return wire.ErrAlreadyServiced
	}

	policy := wire.FunctionPolicy(n.config.MaxClusterRetries, func() error {
		return n.AddCluster(serverAddr, addr)
	}, wire.ExponentialDelay(n.config.ClusterRetryDelay))

	if err := n.addWSClient(serverAddr, conn, hs, policy, true); err != nil {
		conn.Close()
		log.Error(wire.ErrAlreadyServiced, "failed to add websocket cluster")
		log.Red("failed to cluster with network")
		return err
	}

	log.Info("cluster connection established")
	return nil
}

// connectedToMeByAddr returns true/false if we are already connected to server.
func (n *Network) connectedToMeByAddr(addr string) bool {
	n.cu.Lock()
	defer n.cu.Unlock()

	for _, conn := range n.clients {
		if conn.srcInfo == nil {
			continue
		}

		if conn.srcInfo.ServerAddr == addr {
			return true
		}
	}

	return false
}

// connectedToMe returns true/false if we are already connected to server.
func (n *Network) connectedToMe(conn net.Conn) bool {
	n.cu.Lock()
	defer n.cu.Unlock()

	remote := conn.RemoteAddr().String()
	for _, conn := range n.clients {
		if conn.srcInfo == nil {
			continue
		}

		if conn.srcInfo.ServerAddr == remote {
			return true
		}
	}

	return false
}

// AddClient adds a new websocket client through the provided
// net.Conn.
func (n *Network) addClient(serverAddr net.Addr, newConn net.Conn, policy wire.RetryPolicy, isCluster bool) error {
	log := history.WithTitle("Network.addClient").
		With("remote-addr", newConn.RemoteAddr()).
		With("local-addr", newConn.LocalAddr())

	handshake, err := n.config.Upgrader.Upgrade(newConn)
	if err != nil {
		log.Error(err, "Failed to upgrade net.Conn")
		return newConn.Close()
	}

	log.Info("net.Conn upgraded successfully")

	if err := n.addWSClient(serverAddr, newConn, handshake, policy, isCluster); err != nil {
		newConn.Close()
		log.Error(err, "net.Conn failed")
		return err
	}

	return nil
}

func (n *Network) addWSClient(serverAddr net.Addr, conn net.Conn, hs ws.Handshake, policy wire.RetryPolicy, isCluster bool) error {
	log := history.WithFields(history.Attrs{
		"remote-addr": conn.RemoteAddr(),
		"local-addr":  conn.LocalAddr(),
	})

	atomic.AddInt64(&n.totalClients, 1)
	atomic.AddInt64(&n.totalOpened, 1)

	log.Info("Attempting to add net.Conn as websocket server client")

	var wsReader *wsutil.Reader
	var wsWriter *wsutil.Writer

	if isCluster {
		wsReader = wsutil.NewReader(conn, wsClientState)
		wsWriter = wsutil.NewWriter(conn, wsClientState, ws.OpBinary)
	} else {
		wsReader = wsutil.NewReader(conn, wsState)
		wsWriter = wsutil.NewWriterSize(conn, wsState, ws.OpBinary, n.config.MaxPayload)
	}

	client := new(websocketServerClient)
	client.nid = n.id
	client.conn = conn
	client.network = n
	client.wshandshake = hs
	client.wsReader = wsReader
	client.wsWriter = wsWriter
	client.serverAddr = serverAddr
	client.isACluster = isCluster
	client.id = newID(40)
	client.localAddr = conn.LocalAddr()
	client.remoteAddr = conn.RemoteAddr()
	client.parser = new(internal.TaggedMessages)

	client.logs = history.WithFields(map[string]interface{}{
		"server-id":   n.id,
		"client-id":   client.id,
		"server-addr": serverAddr,
		"localAddr":   conn.LocalAddr(),
		"remoteAddr":  conn.RemoteAddr(),
	})

	client.waiter.Add(1)
	go client.readLoop(conn, wsReader)

	var mclient wire.Client
	mclient.NID = n.id
	mclient.ID = client.id
	mclient.CloseFunc = client.close
	mclient.WriteToFunc = client.write
	mclient.WriteFunc = client.writeWithHeader
	mclient.FlushFunc = client.flush
	mclient.LiveFunc = client.isLive
	mclient.InfoFunc = client.getInfo
	mclient.BufferedFunc = client.buffered
	mclient.ReaderFunc = client.serverRead
	mclient.BroadCastFunc = client.broadcast
	mclient.HasPendingFunc = client.hasPending
	mclient.LocalAddrFunc = client.getLocalAddr
	mclient.StatisticFunc = client.getStatistics
	mclient.RemoteAddrFunc = client.getRemoteAddr

	mclient.SiblingsFunc = func() ([]wire.Client, error) {
		return n.getAllClient(client.id)
	}

	// Initial handshake protocol.
	if err := client.handshake(); err != nil {
		return err
	}

	n.cu.Lock()
	n.clients[client.id] = client
	n.cu.Unlock()

	n.routines.Add(1)
	go func(mc wire.Client, addr net.Addr) {
		defer n.routines.Done()

		defer atomic.AddInt64(&n.totalClients, -1)
		defer atomic.AddInt64(&n.totalClosed, 1)
		defer atomic.StoreInt64(&client.closedCounter, 1)

		if err := n.config.Handler(mc); err != nil {
			client.logs.Error(err, "Client connection handler failed")
			log.Red("Client handler stopped")
		}

		n.cu.Lock()
		delete(n.clients, client.id)
		n.cu.Unlock()

		if n.config.Hook != nil {
			if isCluster {
				n.config.Hook.ClusterDisconnected(mclient)
			} else {
				n.config.Hook.NodeDisconnected(mclient)
			}
		}

		if policy != nil {
			go func() {
				log.Info("Client calling retry policy")

				if err := policy.Retry(); err != nil {
					log.Error(err, "Client retry failed")
				}
			}()
		}
	}(mclient, client.remoteAddr)

	if n.config.Hook != nil {
		if isCluster {
			n.config.Hook.ClusterAdded(mclient)
		} else {
			n.config.Hook.NodeAdded(mclient)
		}
	}

	return nil
}

func (n *Network) getAllClient(cid string) ([]wire.Client, error) {
	n.cu.Lock()
	defer n.cu.Unlock()

	var clients []wire.Client
	for _, conn := range n.clients {
		if conn.id == cid {
			continue
		}

		var client wire.Client
		client.NID = n.id
		client.ID = conn.id
		client.LiveFunc = conn.isLive
		client.InfoFunc = conn.getInfo
		client.WriteFunc = conn.writeWithHeader
		client.WriteToFunc = conn.write
		client.FlushFunc = conn.flush
		client.BroadCastFunc = conn.broadcast
		client.BufferedFunc = conn.buffered
		client.HasPendingFunc = conn.hasPending
		client.StatisticFunc = conn.getStatistics
		client.RemoteAddrFunc = conn.getRemoteAddr
		client.LocalAddrFunc = conn.getLocalAddr
		client.SiblingsFunc = func() ([]wire.Client, error) {
			return n.getAllClient(conn.id)
		}

		clients = append(clients, client)
	}

	return clients, nil
}

// Statistics returns statics associated with Network.
func (n *Network) Statistics() wire.NetworkStatistic {
	var stats wire.NetworkStatistic
	stats.ID = n.id
	stats.TotalClients = atomic.LoadInt64(&n.totalClients)
	stats.TotalClosed = atomic.LoadInt64(&n.totalClosed)
	stats.TotalOpened = atomic.LoadInt64(&n.totalOpened)
	return stats
}

// Wait is called to ensure network ended.
func (n *Network) Wait() {
	n.routines.Wait()
}

var alphanums = []rune("bcdfghjklmnpqrstvwxz0123456789")

func newID(length int) string {
	b := make([]rune, length)
	for i := range b {
		b[i] = alphanums[rand.Intn(len(alphanums))]
	}
	return string(b)
}
