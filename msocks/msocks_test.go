package msocks_test

import (
	"bytes"
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io"
	"net"
	"strings"
	"testing"
	"time"

	"github.com/gokit/history"
	"github.com/gokit/history/handlers/std"
	"github.com/influx6/faux/tests"
	"github.com/wirekit/wire"
	"github.com/wirekit/wire/msocks"
)

var (
	_      = history.SetDefaultHandlers(std.Std)
	dialer = &net.Dialer{Timeout: 2 * time.Second}
)

func TestWebsocketServerWithMWebsocketClient(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	netw, err := createNewNetwork(ctx, "localhost:4050", nil)
	if err != nil {
		tests.FailedWithError(err, "Should have successfully created network")
	}
	tests.Passed("Should have successfully created network")

	client, err := msocks.Connect("ws://localhost:4050")
	if err != nil {
		tests.FailedWithError(err, "Should have successfully connected to network")
	}
	tests.Passed("Should have successfully connected to network")

	payload := []byte("pub help")
	_, err = client.Write(payload)
	if err != nil {
		tests.FailedWithError(err, "Should have successfully created new writer")
	}
	tests.Passed("Should have successfully created new writer")

	if ferr := client.Flush(); ferr != nil {
		tests.FailedWithError(ferr, "Should have successfully flush data to network")
	}
	tests.Passed("Should have successfully flush data to network")

	var res []byte
	var readErr error
	for {
		res, readErr = client.Read()
		if readErr != nil && readErr == wire.ErrNoDataYet {
			continue
		}

		break
	}

	if readErr != nil {
		tests.FailedWithError(readErr, "Should have successfully read reply from network")
	}
	tests.Passed("Should have successfully read reply from network")

	expected := []byte("now publishing to [help]\r\n")
	if !bytes.Equal(res, expected) {
		tests.Info("Received: %+q", res)
		tests.Info("Expected: %+q", expected)
		tests.FailedWithError(err, "Should have successfully matched expected data with received from network")
	}
	tests.Passed("Should have successfully matched expected data with received from network")

	if cerr := client.Close(); cerr != nil {
		tests.FailedWithError(cerr, "Should have successfully closed client connection")
	}
	tests.Passed("Should have successfully closed client connection")

	cancel()
	netw.Wait()
}

func TestNetwork_ClusterConnect(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())

	netw, err := createNewNetwork(ctx, "localhost:4050", nil)
	if err != nil {
		tests.FailedWithError(err, "Should have successfully create network")
	}
	tests.Passed("Should have successfully create network")

	netw2, err := createInfoNetwork(ctx, "localhost:7050", nil)
	if err != nil {
		tests.FailedWithError(err, "Should have successfully create network")
	}
	tests.Passed("Should have successfully create network")

	// Should succesfully connect to cluster on localhost:4050
	if err := netw2.AddCluster("localhost:4050"); err != nil {
		tests.FailedWithError(err, "Should have successfully connect to cluster")
	}
	tests.Passed("Should have successfully connect to cluster")

	// Should fail to connect to cluster on localhost:7050 since we are connected already.
	if err := netw.AddCluster("localhost:7050"); err == nil {
		tests.Failed("Should have failed to connect to already connected cluster")
	}
	tests.Passed("Should have failed to connect to already connected cluster")

	cancel()
	netw.Wait()
	netw2.Wait()
}

func TestNetwork_Cluster_Broadcast(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())

	netw, err := createNewNetwork(ctx, "localhost:4050", nil)
	if err != nil {
		tests.FailedWithError(err, "Should have successfully create network")
	}
	tests.Passed("Should have successfully create network")

	netw2, err := createNewNetwork(ctx, "localhost:7050", nil)
	if err != nil {
		tests.FailedWithError(err, "Should have successfully created network")
	}
	tests.Passed("Should have successfully created network")

	// Send net.Conn to be manage by another network manager for talks.
	if err := netw2.AddCluster("localhost:4050"); err != nil {
		tests.FailedWithError(err, "Should have successfully added net.Conn to network")
	}
	tests.Passed("Should have successfully added net.Conn to network")

	client2, err := msocks.Connect("localhost:4050")
	if err != nil {
		tests.FailedWithError(err, "Should have successfully connected to network")
	}
	tests.Passed("Should have successfully connected to network")

	client, err := msocks.Connect("localhost:7050")
	if err != nil {
		tests.FailedWithError(err, "Should have successfully connected to network")
	}
	tests.Passed("Should have successfully connected to network")

	payload := []byte("pub")
	_, err = client.Broadcast(&writer{payload})
	if err != nil {
		tests.FailedWithError(err, "Should have successfully created new writer")
	}
	tests.Passed("Should have successfully created new writer")

	if ferr := client.Flush(); ferr != nil {
		tests.FailedWithError(ferr, "Should have successfully flush data to network")
	}
	tests.Passed("Should have successfully flush data to network")

	var msg []byte
	for {
		msg, err = client2.Read()
		if err != nil {
			if err == wire.ErrNoDataYet {
				err = nil
				continue
			}

		}
		break
	}

	client.Close()
	client2.Close()

	if !bytes.Equal(msg, payload) {
		tests.Failed("Should have received broadcasted message")
	}
	tests.Passed("Should have received broadcasted message")

	cancel()
	netw.Wait()
	netw2.Wait()
}

type writer struct {
	d []byte
}

func (w *writer) WriteTo(wd io.Writer) (int64, error) {
	n, err := wd.Write(w.d)
	return int64(n), err
}

func createNewNetwork(ctx context.Context, addr string, config *tls.Config) (msocks.WebsocketService, error) {
	ll, err := net.Listen("tcp", addr)
	if err != nil {
		return nil, err
	}

	var c msocks.Config
	if config != nil {
		c.Handshake = wire.SimpleServerTLSConfigUpgrader{Config: config}
	}

	c.Handler = func(client wire.Client) error {
		for {
			message, err := client.Read()
			if err != nil {
				if err == wire.ErrNoDataYet {
					time.Sleep(300 * time.Millisecond)
					continue
				}

				return err
			}

			messages := strings.Split(string(message), " ")
			if len(messages) == 0 {
				continue
			}

			command := messages[0]
			rest := messages[1:]

			switch command {
			case "pub":
				res := []byte(fmt.Sprintf("now publishing to %+s\r\n", rest))
				_, err = client.Write(res)
				if err != nil {
					return err
				}
			case "sub":
				res := []byte(fmt.Sprintf("subscribed to %+s\r\n", rest))
				_, err = client.Write(res)
				if err != nil {
					return err
				}
			}

			if err := client.Flush(); err != nil {
				if err == io.ErrShortWrite {
					continue
				}

				return err
			}
		}
	}

	return msocks.ServeListener(ctx, c, ll)
}

func createInfoNetwork(ctx context.Context, addr string, config *tls.Config) (msocks.WebsocketService, error) {
	ll, err := net.Listen("tcp", addr)
	if err != nil {
		return nil, err
	}

	var c msocks.Config
	if config != nil {
		c.Handshake = wire.SimpleServerTLSConfigUpgrader{Config: config}
	}

	c.Handler = func(client wire.Client) error {
		for {
			_, err := client.Read()
			if err != nil {
				if err == wire.ErrNoDataYet {
					time.Sleep(300 * time.Millisecond)
					continue
				}

				return err
			}

			var infos []wire.Info
			infos = append(infos, client.Info())
			if others, err := client.Others(); err == nil {
				for _, item := range others {
					infos = append(infos, item.Info())
				}

				jsn, err := json.Marshal(infos)
				if err != nil {
					return err
				}

				_, err = client.Write(jsn)
				if err != nil {
					return err
				}
			}

			if err := client.Flush(); err != nil {
				if err == io.ErrShortWrite {
					continue
				}

				return err
			}
		}
	}

	return msocks.ServeListener(ctx, c, ll)
}
