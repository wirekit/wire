package mtcp_test

import (
	"context"
	"errors"
	"net"
	"time"

	"bytes"
	"testing"

	"crypto/tls"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"io"
	"strings"

	"github.com/gokit/history"
	"github.com/gokit/history/handlers/std"
	"github.com/influx6/faux/tests"
	"github.com/wirekit/tlsfs/certificates"
	"github.com/wirekit/wire"
	"github.com/wirekit/wire/mtcp"
)

var (
	_      = history.SetDefaultHandlers(std.Std)
	dialer = &net.Dialer{Timeout: 2 * time.Second}
)

func TestNonTLSNetworkWithClient(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	netw, err := createNewNetwork(ctx, "localhost:4050", nil)
	if err != nil {
		tests.FailedWithError(err, "Should have successfully create network")
	}
	tests.Passed("Should have successfully create network")

	client, err := mtcp.Connect(mtcp.ClientConfig{Addr: "localhost:4050"})
	if err != nil {
		tests.FailedWithError(err, "Should have successfully connected to network")
	}
	tests.Passed("Should have successfully connected to network")

	payload := []byte("pub help")
	_, err = client.Stream(&writer{d: payload})
	if err != nil {
		tests.FailedWithError(err, "Should have successfully created new writer")
	}
	tests.Passed("Should have successfully created new writer")

	if ferr := client.Flush(); ferr != nil {
		tests.FailedWithError(ferr, "Should have successfully flush data to network")
	}
	tests.Passed("Should have successfully flush data to network")

	var res []byte
	var readErr error
	for {
		res, readErr = client.Read()
		if readErr != nil && readErr == wire.ErrNoDataYet {
			continue
		}

		break
	}

	if readErr != nil {
		tests.FailedWithError(readErr, "Should have successfully read reply from network")
	}
	tests.Passed("Should have successfully read reply from network")

	expected := []byte("now publishing to [help]\r\n")
	if !bytes.Equal(res, expected) {
		tests.Info("Received: %+q", res)
		tests.Info("Expected: %+q", expected)
		tests.FailedWithError(err, "Should have successfully matched expected data with received from network")
	}
	tests.Passed("Should have successfully matched expected data with received from network")

	if cerr := client.Close(); cerr != nil {
		tests.FailedWithError(cerr, "Should have successfully closed client connection")
	}
	tests.Passed("Should have successfully closed client connection")

	cancel()
	netw.Wait()
}

func TestTLSNetworkWithClient(t *testing.T) {
	_, serverca, clientca, err := createTLSCA()
	if err != nil {
		tests.FailedWithError(err, "Should have successfully created server and client certs")
	}
	tests.Passed("Should have successfully created server and client certs")

	serverTls, err := serverca.TLSServerConfig(true)
	if err != nil {
		tests.FailedWithError(err, "Should have successfully create sever's tls config")
	}
	tests.Passed("Should have successfully create sever's tls config")

	clientTls, err := clientca.TLSClientConfig()
	if err != nil {
		tests.FailedWithError(err, "Should have successfully create sever's tls config")
	}
	tests.Passed("Should have successfully create sever's tls config")

	ctx, cancel := context.WithCancel(context.Background())
	netw, err := createNewNetwork(ctx, "localhost:4050", serverTls)
	if err != nil {
		tests.FailedWithError(err, "Should have successfully create network")
	}
	tests.Passed("Should have successfully create network")

	client, err := mtcp.Connect(mtcp.ClientConfig{
		Addr:       "localhost:4050",
		DomainName: "localhost",
		Handshake: wire.SimpleClientTLSConfigUpgrader{
			Config: clientTls,
		},
	})

	if err != nil {
		tests.FailedWithError(err, "Should have successfully connected to network")
	}
	tests.Passed("Should have successfully connected to network")

	payload := []byte("pub help")
	_, err = client.Write(payload)
	if err != nil {
		tests.FailedWithError(err, "Should have successfully created new writer")
	}
	tests.Passed("Should have successfully created new writer")

	if ferr := client.Flush(); ferr != nil {
		tests.FailedWithError(ferr, "Should have successfully flush data to network")
	}
	tests.Passed("Should have successfully flush data to network")

	var res []byte
	var readErr error
	for {
		res, readErr = client.Read()
		if readErr != nil && readErr == wire.ErrNoDataYet {
			continue
		}

		break
	}

	if readErr != nil {
		tests.FailedWithError(readErr, "Should have successfully read reply from network")
	}
	tests.Passed("Should have successfully read reply from network")

	expected := []byte("now publishing to [help]\r\n")
	if !bytes.Equal(res, expected) {
		tests.Info("Received: %+q", res)
		tests.Info("Expected: %+q", expected)
		tests.FailedWithError(err, "Should have successfully matched expected data with received from network")
	}
	tests.Passed("Should have successfully matched expected data with received from network")

	if cerr := client.Close(); cerr != nil {
		tests.FailedWithError(cerr, "Should have successfully closed client connection")
	}
	tests.Passed("Should have successfully closed client connection")

	cancel()
	netw.Wait()
}

func TestNetwork_Cluster_Broadcast(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())

	netw, err := createNewNetwork(ctx, "localhost:4050", nil)
	if err != nil {
		tests.FailedWithError(err, "Should have successfully create network")
	}
	tests.Passed("Should have successfully create network")

	netw2, err := createNewNetwork(ctx, "localhost:7050", nil)
	if err != nil {
		tests.FailedWithError(err, "Should have successfully created network")
	}
	tests.Passed("Should have successfully created network")

	// Send net.Conn to be manage by another network manager for talks.
	if err := netw2.AddCluster("localhost:4050"); err != nil {
		tests.FailedWithError(err, "Should have successfully added net.Conn to network")
	}
	tests.Passed("Should have successfully added net.Conn to network")

	client2, err := mtcp.Connect(mtcp.ClientConfig{Addr: "localhost:4050"})
	if err != nil {
		tests.FailedWithError(err, "Should have successfully connected to network")
	}
	tests.Passed("Should have successfully connected to network")

	client, err := mtcp.Connect(mtcp.ClientConfig{Addr: "localhost:7050"})
	if err != nil {
		tests.FailedWithError(err, "Should have successfully connected to network")
	}
	tests.Passed("Should have successfully connected to network")

	payload := []byte("pub")
	_, err = client.Broadcast(&writer{d: payload})
	if err != nil {
		tests.FailedWithError(err, "Should have successfully created new writer")
	}
	tests.Passed("Should have successfully created new writer")

	if ferr := client.Flush(); ferr != nil {
		tests.FailedWithError(ferr, "Should have successfully flush data to network")
	}
	tests.Passed("Should have successfully flush data to network")

	var msg []byte
	for {
		msg, err = client2.Read()
		if err != nil {
			if err == wire.ErrNoDataYet {
				err = nil
				continue
			}

		}
		break
	}

	client.Close()
	client2.Close()

	if !bytes.Equal(msg, payload) {
		tests.Failed("Should have received broadcasted message")
	}
	tests.Passed("Should have received broadcasted message")

	cancel()
	netw.Wait()
	netw2.Wait()
}

func TestNetwork_ClusterConnect(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())

	netw, err := createNewNetwork(ctx, "localhost:4050", nil)
	if err != nil {
		tests.FailedWithError(err, "Should have successfully create network")
	}
	tests.Passed("Should have successfully create network")

	netw2, err := createInfoNetwork(ctx, "localhost:7050", nil)
	if err != nil {
		tests.FailedWithError(err, "Should have successfully create network")
	}
	tests.Passed("Should have successfully create network")

	// Should succesfully connect to cluster on localhost:4050
	if err := netw2.AddCluster("localhost:4050"); err != nil {
		tests.FailedWithError(err, "Should have successfully connect to cluster")
	}
	tests.Passed("Should have successfully connect to cluster")

	// Should fail to connect to cluster on localhost:7050 since we are connected already.
	if err := netw.AddCluster("localhost:7050"); err == nil {
		tests.Failed("Should have failed to connect to already connected cluster")
	}
	tests.Passed("Should have failed to connect to already connected cluster")

	cancel()
	netw.Wait()
	netw2.Wait()
}

type writer struct {
	d []byte
}

func (w *writer) WriteTo(wd io.Writer) (int64, error) {
	n, err := wd.Write(w.d)
	return int64(n), err
}

func readMessage(conn net.Conn) ([]byte, error) {
	incoming := make([]byte, 4)
	n, err := conn.Read(incoming)
	if err != nil {
		return nil, err
	}

	expectedLen := binary.BigEndian.Uint32(incoming[:n])
	data := make([]byte, expectedLen)
	n, err = conn.Read(data)
	if err != nil {
		return nil, err
	}

	if n != int(expectedLen) {
		return data, errors.New("expected length unmarched")
	}

	return data[:n], nil
}

func makeMessage(msg []byte) []byte {
	header := make([]byte, 4, len(msg)+4)
	binary.BigEndian.PutUint32(header, uint32(len(msg)))
	header = append(header, msg...)
	return header
}

func createTLSCA() (ca certificates.CertificateAuthority, server, client certificates.CertificateRequest, err error) {
	service := certificates.CertificateAuthorityProfile{
		CommonName:   "*",
		Local:        "Lagos",
		Organization: "DreamBench",
		Country:      "Nigeria",
		Province:     "South-West",
	}

	service.RSAKeyStrength = 4096
	service.LifeTime = (time.Hour * 8760)
	service.Emails = append([]string{}, "alex.ewetumo@dreambench.io")

	var requestService certificates.CertificateRequestProfile
	requestService.RSAKeyStrength = 2048
	requestService.CommonName = "*"

	ca, err = certificates.CreateCertificateAuthority(service)
	if err != nil {
		return
	}

	if server, err = certificates.CreateCertificateRequest(requestService); err == nil {
		if err = ca.ApproveServerCertificateSigningRequest(&server, time.Hour*8760); err != nil {
			return
		}
	}

	if client, err = certificates.CreateCertificateRequest(requestService); err == nil {
		if err = ca.ApproveClientCertificateSigningRequest(&client, time.Hour*8760); err != nil {
			return
		}
	}

	return
}

func createNewNetwork(ctx context.Context, addr string, config *tls.Config) (mtcp.TCPService, error) {
	ll, err := net.Listen("tcp", addr)
	if err != nil {
		return nil, err
	}

	var c mtcp.Config
	if config != nil {
		c.Handshake = wire.SimpleServerTLSConfigUpgrader{Config: config}
	}

	c.Handler = func(client wire.Client) error {
		for {
			message, err := client.Read()
			if err != nil {
				if err == wire.ErrNoDataYet {
					time.Sleep(300 * time.Millisecond)
					continue
				}

				return err
			}

			messages := strings.Split(string(message), " ")
			if len(messages) == 0 {
				continue
			}

			command := messages[0]
			rest := messages[1:]

			switch command {
			case "pub":
				res := []byte(fmt.Sprintf("now publishing to %+s\r\n", rest))
				_, err := client.Write(res)
				if err != nil {
					return err
				}
			case "sub":
				res := []byte(fmt.Sprintf("subscribed to %+s\r\n", rest))
				_, err := client.Write(res)
				if err != nil {
					return err
				}
			}

			if err := client.Flush(); err != nil {
				if err == io.ErrShortWrite {
					continue
				}

				return err
			}
		}
	}

	return mtcp.ServeListener(ctx, c, ll)
}

func createInfoNetwork(ctx context.Context, addr string, config *tls.Config) (mtcp.TCPService, error) {
	ll, err := net.Listen("tcp", addr)
	if err != nil {
		return nil, err
	}

	var c mtcp.Config
	if config != nil {
		c.Handshake = wire.SimpleServerTLSConfigUpgrader{Config: config}
	}

	c.Handler = func(client wire.Client) error {
		for {
			_, err := client.Read()
			if err != nil {
				if err == wire.ErrNoDataYet {
					time.Sleep(300 * time.Millisecond)
					continue
				}

				return err
			}

			var infos []wire.Info
			infos = append(infos, client.Info())

			if others, err := client.Others(); err == nil {
				for _, item := range others {
					infos = append(infos, item.Info())
				}

				jsn, err := json.Marshal(infos)
				if err != nil {
					return err
				}

				_, err = client.Write(jsn)
				if err != nil {
					return err
				}
			}

			if err := client.Flush(); err != nil {
				if err == io.ErrShortWrite {
					continue
				}

				return err
			}
		}
	}

	return mtcp.ServeListener(ctx, c, ll)
}
