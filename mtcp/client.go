package mtcp

import (
	"bufio"
	"bytes"
	"errors"
	"io"
	"net"
	"sync"
	"sync/atomic"
	"time"

	"encoding/json"

	"context"

	"encoding/binary"

	"reflect"
	"unsafe"

	"github.com/gokit/history"
	"github.com/wirekit/llio"
	"github.com/wirekit/wire"
	"github.com/wirekit/wire/internal"
)

// ClientConfig provides properties for initiating tcp network.
type ClientConfig struct {
	// Addr provides the address of the network to be connected
	// to.
	Addr string

	// DomainName provides the domain name to be used if ClientConfig.Handshake
	// is provided.
	DomainName string

	// MaxPayloadSize defines the total size to be given to
	// the connection buffer writer.
	MaxPayload int

	// MaxBuffer sets the maximum allowed buffer for read/writes.
	MaxBuffer int

	// Handshake provides implementation for processing
	// connections tls handshake.
	Handshake wire.TLSHandshake

	// Dialer to be used to create net.Conn to connect to cluster address
	// in Network.AddCluster. Set to use a custom dialer.
	Dialer *net.Dialer
}

func (c *ClientConfig) init() error {
	if c.Addr == "" {
		return errors.New("ClientConfig.Addr is required")
	}

	if c.Handshake != nil {
		if c.DomainName == "" {
			return errors.New("ClientConfig.Domain is required when ClientConfig.Handshake is supplied")
		}
	}

	if c.Handshake == nil {
		c.Handshake = wire.NoUpgrade{}
	}

	if c.MaxPayload <= 0 {
		c.MaxPayload = wire.MaxPayloadSize
	}

	if c.MaxBuffer <= 0 {
		c.MaxBuffer = wire.MaxBufferSize
	}

	if c.Dialer == nil {
		c.Dialer = &net.Dialer{
			KeepAlive: wire.DefaultKeepAlive,
			Timeout:   wire.DefaultDialTimeout,
		}
	}

	return nil
}

// Connect is used to implement the client connection to connect to a
// mtcp.Network. It implements all the method functions required
// by the Client to communicate with the server. It understands
// the message length header sent along by every message and follows
// suite when sending to server.
func Connect(cc ClientConfig) (wire.Client, error) {
	var c wire.Client
	if err := cc.init(); err != nil {
		return c, err
	}

	c.ID = newID(40)

	network := new(clientNetwork)
	c.NID = network.nid

	atomic.StoreInt64(&network.maxPayload, int64(cc.MaxPayload))

	network.totalReconnects = -1
	network.id = c.ID
	network.cc = cc
	network.parser = new(internal.TaggedMessages)
	network.logs = history.WithTags("mtcp-client").WithFields(map[string]interface{}{
		"id":   c.ID,
		"addr": cc.Addr,
	})

	c.LiveFunc = network.isLive
	c.FlushFunc = network.flush
	c.ReaderFunc = network.read
	c.WriteToFunc = network.write
	c.WriteFunc = network.writeWithHeader
	c.CloseFunc = network.close
	c.BufferedFunc = network.buffered
	c.BroadCastFunc = network.broadcast
	c.HasPendingFunc = network.hasPending
	c.LocalAddrFunc = network.getLocalAddr
	c.RemoteAddrFunc = network.getRemoteAddr
	c.ReconnectionFunc = network.reconnect
	c.StatisticFunc = network.getStatistics

	if err := network.reconnect(cc.Addr); err != nil {
		return c, err
	}

	return c, nil
}

type clientNetwork struct {
	cc              ClientConfig
	maxPayload      int64
	totalRead       int64
	totalWritten    int64
	totalFlushed    int64
	MessageRead     int64
	MessageWritten  int64
	totalReconnects int64
	totalMisses     int64
	closed          int64

	sim        sync.Mutex
	serverInfo *wire.Info

	logs   history.Ctx
	parser *internal.TaggedMessages

	bu         sync.Mutex
	buffWriter *bufio.Writer

	id  string
	nid string

	localAddr  net.Addr
	remoteAddr net.Addr
	do         sync.Once

	cu   sync.RWMutex
	conn net.Conn

	worker sync.WaitGroup
}

func (cn *clientNetwork) maxPayloadLength() int {
	return int(atomic.LoadInt64(&cn.maxPayload))
}

func (nc *clientNetwork) handleCapacityUpdate(d []byte) error {
	d = bytes.TrimPrefix(d, healthBytes)
	return nil
}

func (cn *clientNetwork) handleCINFO() error {
	jsn, err := json.Marshal(cn.getInfo())
	if err != nil {
		return err
	}

	bu := guardPool.Get().(*bytes.Buffer)
	bu.Reset()
	defer guardPool.Put(bu)

	bu.Write(rinfoBytes)
	bu.Write(jsn)

	_, err = cn.writeSize(bu.Len(), bu)
	if err != nil {
		return err
	}

	return cn.flush()
}

func (cn *clientNetwork) handleRINFO(data []byte) error {
	data = bytes.TrimPrefix(data, rinfoBytes)

	var info wire.Info
	if err := json.Unmarshal(data, &info); err != nil {
		return err
	}

	cn.sim.Lock()
	cn.serverInfo = &info
	cn.sim.Unlock()

	atomic.StoreInt64(&cn.maxPayload, info.MaxPayload)

	return nil
}

func (cn *clientNetwork) sendHandshakeSkip() error {
	if _, err := cn.writeWithHeader(handshakeSkipBytes); err != nil {
		return err
	}

	return cn.flush()
}

func (cn *clientNetwork) getInfo() wire.Info {
	addr := cn.cc.Addr
	if cn.remoteAddr != nil {
		addr = cn.remoteAddr.String()
	}

	return wire.Info{
		ID:         cn.id,
		ServerAddr: addr,
		MaxPayload: int64(cn.cc.MaxPayload),
	}
}

func (cn *clientNetwork) getStatistics() (wire.ClientStatistic, error) {
	var stats wire.ClientStatistic
	stats.ID = cn.id
	stats.Local = cn.localAddr
	stats.Remote = cn.remoteAddr
	stats.BytesRead = atomic.LoadInt64(&cn.totalRead)
	stats.MessagesRead = atomic.LoadInt64(&cn.MessageRead)
	stats.BytesWritten = atomic.LoadInt64(&cn.totalWritten)
	stats.BytesFlushed = atomic.LoadInt64(&cn.totalFlushed)
	stats.Reconnects = atomic.LoadInt64(&cn.totalReconnects)
	stats.MessagesWritten = atomic.LoadInt64(&cn.MessageWritten)
	return stats, nil
}

// isLive returns error if clientNetwork is not connected to remote network.
func (cn *clientNetwork) isLive() error {
	if atomic.LoadInt64(&cn.closed) == 1 {
		return wire.ErrAlreadyClosed
	}
	return nil
}

func (cn *clientNetwork) getRemoteAddr() (net.Addr, error) {
	cn.cu.RLock()
	defer cn.cu.RUnlock()
	return cn.remoteAddr, nil
}

func (cn *clientNetwork) getLocalAddr() (net.Addr, error) {
	cn.cu.RLock()
	defer cn.cu.RUnlock()
	return cn.localAddr, nil
}

func (cn *clientNetwork) hasPending() bool {
	if err := cn.isLive(); err != nil {
		return false
	}

	cn.bu.Lock()
	defer cn.bu.Unlock()
	if cn.buffWriter == nil {
		return false
	}

	return cn.buffWriter.Buffered() > 0
}

func (cn *clientNetwork) bufferedWith(add int) (float64, error) {
	if err := cn.isLive(); err != nil {
		return 0, err
	}

	cn.bu.Lock()
	defer cn.bu.Unlock()
	if cn.buffWriter == nil {
		return 0, wire.ErrAlreadyClosed
	}

	available := float64(cn.buffWriter.Available())
	buffered := float64(cn.buffWriter.Buffered() + add)
	return buffered / available, nil
}

func (cn *clientNetwork) buffered() (float64, error) {
	if err := cn.isLive(); err != nil {
		return 0, err
	}

	cn.bu.Lock()
	defer cn.bu.Unlock()
	if cn.buffWriter == nil {
		return 0, wire.ErrAlreadyClosed
	}

	available := float64(cn.buffWriter.Available())
	buffered := float64(cn.buffWriter.Buffered())
	return buffered / available, nil
}

func (cn *clientNetwork) flush() error {
	if err := cn.isLive(); err != nil {
		return err
	}

	var conn net.Conn
	cn.cu.RLock()
	conn = cn.conn
	cn.cu.RUnlock()

	if conn == nil {
		return wire.ErrAlreadyClosed
	}

	cn.bu.Lock()
	defer cn.bu.Unlock()

	if cn.buffWriter == nil {
		return wire.ErrAlreadyClosed
	}

	available := cn.buffWriter.Buffered()
	atomic.StoreInt64(&cn.totalFlushed, int64(available))

	conn.SetWriteDeadline(time.Now().Add(wire.MaxFlushDeadline))
	err := cn.buffWriter.Flush()
	if err != nil {
		conn.SetWriteDeadline(time.Time{})
		return err
	}
	conn.SetWriteDeadline(time.Time{})
	return nil
}

func (cn *clientNetwork) broadcast(w io.WriterTo) (int64, error) {
	bu := guardPool.Get().(*bytes.Buffer)
	bu.Reset()
	defer guardPool.Put(bu)

	bu.Write(broadcastBytes)
	if n, err := w.WriteTo(bu); err != nil {
		return n, err
	}

	m, err := cn.writeSize(bu.Len(), bu)
	if err != nil {
		return m, err
	}

	return m, nil
}

func (cn *clientNetwork) write(w io.WriterTo) (int64, error) {
	if err := cn.isLive(); err != nil {
		return 0, err
	}

	var conn net.Conn
	cn.cu.RLock()
	conn = cn.conn
	cn.cu.RUnlock()

	if conn == nil {
		return 0, wire.ErrAlreadyClosed
	}

	bu := guardPool.Get().(*bytes.Buffer)
	defer guardPool.Put(bu)

	n, err := w.WriteTo(bu)
	if err != nil {
		return n, err
	}

	if int(n) >= cn.cc.MaxPayload {
		return n, wire.ErrToBig
	}

	return cn.writeSize(bu.Len(), bu)
}

func (cn *clientNetwork) writeSize(size int, bu io.WriterTo) (int64, error) {
	// steal 4 bytes from stack, we dont want to allocate here.
	var inco [4]byte
	incoPtr := uintptr(unsafe.Pointer(&inco))
	incoHeader := &reflect.SliceHeader{Data: incoPtr, Len: 4, Cap: 4}
	area := *(*[]byte)(unsafe.Pointer(incoHeader))

	cn.bu.Lock()
	defer cn.bu.Unlock()

	if cn.buffWriter == nil {
		return 0, wire.ErrAlreadyClosed
	}

	var shouldFlush bool
	buffered := cn.buffWriter.Buffered()
	available := cn.buffWriter.Available()

	if buffered+4+size > available {
		shouldFlush = true
	}

	binary.BigEndian.PutUint32(area, uint32(size))
	cn.buffWriter.Write(area)

	n, err := bu.WriteTo(cn.buffWriter)
	if err != nil {
		return n, err
	}

	if shouldFlush {
		err = cn.buffWriter.Flush()
	}

	return n, nil
}

func (cn *clientNetwork) writeWithHeader(d []byte) (int, error) {
	area := bit4Pool.Get().([]byte)
	defer bit4Pool.Put(area)

	cn.bu.Lock()
	defer cn.bu.Unlock()

	if cn.buffWriter == nil {
		return 0, wire.ErrAlreadyClosed
	}

	size := len(d)

	var shouldFlush bool
	buffered := cn.buffWriter.Buffered()
	available := cn.buffWriter.Available()

	if buffered+4+size > available {
		shouldFlush = true
	}

	binary.BigEndian.PutUint32(area, uint32(size))
	cn.buffWriter.Write(area)
	n, err := cn.buffWriter.Write(d)
	if err != nil {
		return n, err
	}

	if shouldFlush {
		err = cn.buffWriter.Flush()
	}

	return n, err
}

func (cn *clientNetwork) read() ([]byte, error) {
	if err := cn.isLive(); err != nil {
		atomic.AddInt64(&cn.MessageRead, 1)
		if indata, cerr := cn.parser.Next(); cerr == nil {
			return indata, err
		}

		return nil, err
	}

	indata, err := cn.parser.Next()
	atomic.AddInt64(&cn.totalRead, int64(len(indata)))
	if err != nil {
		return nil, err
	}

	atomic.AddInt64(&cn.MessageRead, 1)

	// if its a broadcast, just trim the header out.
	if bytes.HasPrefix(indata, broadcastBytes) {
		return bytes.TrimPrefix(indata, broadcastBytes), nil
	}

	if bytes.HasPrefix(indata, healthBytes) {
		if err := cn.handleCapacityUpdate(indata); err != nil {
			return nil, err
		}
		return nil, wire.ErrNoDataYet
	}

	if bytes.HasPrefix(indata, rinfoBytes) {
		if err := cn.handleRINFO(indata); err != nil {
			return nil, err
		}
		return nil, wire.ErrNoDataYet
	}

	if bytes.HasPrefix(indata, cinfoBytes) {
		if err := cn.handleCINFO(); err != nil {
			return nil, err
		}
		return nil, wire.ErrNoDataYet
	}

	return indata, nil
}

func (cn *clientNetwork) readLoop(conn net.Conn) {
	defer cn.close()
	defer cn.worker.Done()

	connReader := bufio.NewReaderSize(conn, cn.cc.MaxBuffer)
	lreader := llio.NewLengthRecvReader(connReader)

	var incoming []byte

	for {
		frame, err := lreader.ReadHeader()
		if err != nil {
			cn.logs.Error(err, "read header error")
			return
		}

		if frame > cn.cc.MaxPayload {
			cn.logs.Error(err, "exceeded allowed payload size")
			return
		}

		incoming = make([]byte, frame)
		_, err = lreader.Read(incoming)
		if err != nil {
			cn.logs.Error(err, "read body error")
			return
		}

		atomic.AddInt64(&cn.totalRead, int64(frame))

		cn.logs.WithFields(history.Attrs{
			"read":  string(incoming),
			"total": frame,
		}).Info("received data")

		// Turn slice into pointer, so we can ensure to avoid
		// heap allocation.
		incoPtr := unsafe.Pointer(&incoming)
		if err := cn.parser.ParseWithPtr(incoPtr); err != nil {
			cn.logs.Error(err, "parser data error")
			return
		}
	}
}

func (cn *clientNetwork) sendHLTReq() error {
	if _, err := cn.writeWithHeader(healthBytes); err != nil {
		return err
	}

	if err := cn.flush(); err != nil {
		return err
	}

	return nil
}

func (cn *clientNetwork) sendCINFOReq() error {
	// Send to new client wire.CINFO request
	if _, err := cn.writeWithHeader(cinfoBytes); err != nil {
		return err
	}

	if err := cn.flush(); err != nil {
		return err
	}

	return nil
}

func (cn *clientNetwork) close() error {
	if err := cn.isLive(); err != nil {
		return err
	}

	cn.flush()

	cn.cu.Lock()
	if cn.conn == nil {
		return wire.ErrAlreadyClosed
	}
	cn.conn.Close()
	cn.cu.Unlock()

	atomic.StoreInt64(&cn.closed, 1)

	cn.worker.Wait()

	cn.cu.Lock()
	cn.conn = nil
	cn.cu.Unlock()

	cn.bu.Lock()
	cn.buffWriter = nil
	cn.bu.Unlock()

	return nil
}

func (cn *clientNetwork) reconnect(altAddr string) error {
	if err := cn.isLive(); err != nil {
		return err
	}

	atomic.AddInt64(&cn.totalReconnects, 1)

	// ensure we really have stopped loop.
	cn.worker.Wait()

	var err error
	var conn net.Conn

	// First we test out the alternate address, to see if we get a connection.
	// If we get no connection, then attempt to dial original address and finally
	// return error.
	if altAddr != "" {
		if conn, err = cn.getConn(altAddr); err != nil {
			conn, err = cn.getConn(cn.cc.Addr)
		}
	} else {
		conn, err = cn.getConn(cn.cc.Addr)
	}

	// If failure was met, then return error and go-offline again.
	if err != nil {
		return err
	}

	atomic.StoreInt64(&cn.closed, 0)

	cn.bu.Lock()
	if cn.buffWriter == nil {
		cn.buffWriter = bufio.NewWriterSize(conn, cn.cc.MaxBuffer)
	} else {
		cn.buffWriter.Reset(conn)
	}
	cn.bu.Unlock()

	cn.cu.Lock()
	cn.do = sync.Once{}
	cn.conn = conn
	cn.localAddr = conn.LocalAddr()
	cn.remoteAddr = conn.RemoteAddr()
	cn.cu.Unlock()

	cn.logs.With("local-addr", cn.localAddr)
	cn.logs.With("remote-addr", cn.remoteAddr)
	cn.logs.With("addr", cn.remoteAddr.String())

	// We are client so skip handshake process.
	if err := cn.sendHandshakeSkip(); err != nil {
		return err
	}

	if err := cn.sendCINFOReq(); err != nil {
		return err
	}

	cn.worker.Add(1)
	go cn.readLoop(conn)

	return nil
}

// getConn returns net.Conn for giving addr.
func (cn *clientNetwork) getConn(addr string) (net.Conn, error) {
	lastSleep := wire.MinTemporarySleep

	var err error
	var conn net.Conn

	for {
		conn, err = cn.cc.Dialer.Dial("tcp", addr)
		if err != nil {
			if netErr, ok := err.(net.Error); ok && netErr.Temporary() {
				if lastSleep >= wire.MaxTemporarySleep {
					return nil, err
				}

				time.Sleep(lastSleep)
				lastSleep *= 2
			}
			continue
		}
		break
	}

	if cn.cc.Handshake != nil {
		return cn.cc.Handshake.Upgrade(context.Background(), cn.cc.DomainName, conn)
	}

	return conn, nil
}
