package mtcp

import (
	"bufio"
	"errors"
	"net"
	"sync"
	"sync/atomic"
	"time"

	"io"

	"bytes"

	"encoding/json"

	"context"

	"math/rand"

	"encoding/binary"

	"reflect"
	"unsafe"

	"github.com/gokit/history"
	"github.com/wirekit/llio"
	"github.com/wirekit/wire"
	"github.com/wirekit/wire/internal"
)

var (
	cinfoBytes                    = []byte(wire.CINFO)
	rinfoBytes                    = []byte(wire.RINFO)
	clStatusBytes                 = []byte(wire.CLSTATUS)
	rescueBytes                   = []byte(wire.CRESCUE)
	healthBytes                   = []byte(wire.HLTINFO)
	handshakeCompletedBytes       = []byte(wire.CLHANDSHAKECOMPLETED)
	clientHandshakeCompletedBytes = []byte(wire.ClientHandShakeCompleted)
	handshakeSkipBytes            = []byte(wire.CLHandshakeSkip)
	broadcastBytes                = []byte(wire.BROADCAST)
)

var (
	bit4Pool = sync.Pool{New: func() interface{} {
		return make([]byte, 4)
	}}

	guardPool = sync.Pool{New: func() interface{} {
		return new(bytes.Buffer)
	}}
)

//************************************************************************
//  TCP client on Server Implementation
//************************************************************************

type tcpServerClient struct {
	isACluster bool
	nid        string
	id         string
	serverAddr net.Addr
	localAddr  net.Addr
	remoteAddr net.Addr
	config     Config
	srcInfo    *wire.Info
	logs       history.Ctx
	network    *Network
	worker     sync.WaitGroup
	do         sync.Once

	parser *internal.TaggedMessages

	totalRead      int64
	totalWritten   int64
	totalFlushOut  int64
	totalWriteMsgs int64
	totalReadMsgs  int64
	closed         int64

	reader *llio.LengthRecvReader
	sos    *guardedBuffer

	mu   sync.RWMutex
	conn net.Conn

	bu         sync.Mutex
	buffWriter *bufio.Writer
}

// isCluster returns true/false if a giving connection is actually
// connected to another server i.e a server-to-server and not a
// client-to-server connection.
func (nc *tcpServerClient) isCluster() bool {
	return nc.isACluster
}

// isLive returns an error if networkconn is disconnected from network.
func (nc *tcpServerClient) isLive() error {
	if atomic.LoadInt64(&nc.closed) == 1 {
		return wire.ErrAlreadyClosed
	}

	nc.mu.Lock()
	if nc.conn == nil {
		nc.mu.Unlock()
		return wire.ErrAlreadyClosed
	}
	nc.mu.Unlock()

	return nil
}

func (nc *tcpServerClient) getStatistics() (wire.ClientStatistic, error) {
	var stats wire.ClientStatistic
	stats.ID = nc.id
	stats.Local = nc.localAddr
	stats.Remote = nc.remoteAddr
	stats.BytesRead = atomic.LoadInt64(&nc.totalRead)
	stats.BytesFlushed = atomic.LoadInt64(&nc.totalFlushOut)
	stats.BytesWritten = atomic.LoadInt64(&nc.totalWritten)
	stats.MessagesRead = atomic.LoadInt64(&nc.totalReadMsgs)
	stats.MessagesWritten = atomic.LoadInt64(&nc.totalWriteMsgs)
	return stats, nil
}

func (nc *tcpServerClient) hasPending() bool {
	if err := nc.isLive(); err != nil {
		return false
	}

	nc.bu.Lock()
	defer nc.bu.Unlock()
	if nc.buffWriter == nil {
		return false
	}

	return nc.buffWriter.Buffered() > 0
}

func (nc *tcpServerClient) buffered() (float64, error) {
	if err := nc.isLive(); err != nil {
		return 0, err
	}

	nc.bu.Lock()
	defer nc.bu.Unlock()

	if nc.buffWriter == nil {
		return 0, wire.ErrAlreadyClosed
	}

	available := float64(nc.buffWriter.Available())
	buffered := float64(nc.buffWriter.Buffered())
	return buffered / available, nil
}

func (nc *tcpServerClient) flush() error {
	if err := nc.isLive(); err != nil {
		return err
	}

	var conn net.Conn
	nc.mu.Lock()
	conn = nc.conn
	nc.mu.Unlock()

	nc.bu.Lock()
	defer nc.bu.Unlock()
	if nc.buffWriter == nil {
		return wire.ErrAlreadyClosed
	}

	buffered := nc.buffWriter.Buffered()
	atomic.AddInt64(&nc.totalFlushOut, int64(buffered))

	conn.SetWriteDeadline(time.Now().Add(wire.MaxFlushDeadline))
	err := nc.buffWriter.Flush()
	conn.SetWriteDeadline(time.Time{})
	return err
}

func (nc *tcpServerClient) write(w io.WriterTo) (int64, error) {
	if err := nc.isLive(); err != nil {
		return 0, err
	}

	bu := guardPool.Get().(*bytes.Buffer)
	bu.Reset()
	defer guardPool.Put(bu)

	n, err := w.WriteTo(bu)
	if err != nil {
		return n, err
	}

	if int(n) >= nc.config.MaxPayload {
		return n, wire.ErrToBig
	}

	return nc.writeSize(bu.Len(), bu)
}

func (nc *tcpServerClient) writeSize(size int, bu io.WriterTo) (int64, error) {
	// steal 4 bytes from stack, we dont want to allocate here.
	var inco [4]byte
	incoPtr := uintptr(unsafe.Pointer(&inco))
	incoHeader := &reflect.SliceHeader{Data: incoPtr, Len: 4, Cap: 4}
	area := *(*[]byte)(unsafe.Pointer(incoHeader))

	nc.bu.Lock()
	defer nc.bu.Unlock()

	if nc.buffWriter == nil {
		return 0, wire.ErrAlreadyClosed
	}

	var shouldFlush bool
	buffered := nc.buffWriter.Buffered()
	available := nc.buffWriter.Available()

	if buffered+4+size > available {
		shouldFlush = true
	}

	binary.BigEndian.PutUint32(area, uint32(size))
	nc.buffWriter.Write(area)

	n, err := bu.WriteTo(nc.buffWriter)
	if err != nil {
		return n, err
	}

	if shouldFlush {
		err = nc.buffWriter.Flush()
	}

	return n, nil
}

func (nc *tcpServerClient) writeWithHeader(d []byte) (int, error) {
	// steal 4 bytes from stack, we dont want to allocate here.
	var inco [4]byte
	incoPtr := uintptr(unsafe.Pointer(&inco))
	incoHeader := &reflect.SliceHeader{Data: incoPtr, Len: 4, Cap: 4}
	area := *(*[]byte)(unsafe.Pointer(incoHeader))

	nc.bu.Lock()
	defer nc.bu.Unlock()

	if nc.buffWriter == nil {
		return 0, wire.ErrAlreadyClosed
	}

	size := len(d)

	var shouldFlush bool
	buffered := nc.buffWriter.Buffered()
	available := nc.buffWriter.Available()

	if buffered+4+size > available {
		shouldFlush = true
	}

	binary.BigEndian.PutUint32(area, uint32(size))
	nc.buffWriter.Write(area)
	n, err := nc.buffWriter.Write(d)
	if err != nil {
		return n, err
	}

	if shouldFlush {
		err = nc.buffWriter.Flush()
	}

	return n, err
}

func (nc *tcpServerClient) broadcast(w io.WriterTo) (int64, error) {
	if err := nc.isLive(); err != nil {
		return 0, err
	}

	bu := guardPool.Get().(*bytes.Buffer)
	bu.Reset()
	defer guardPool.Put(bu)

	n, err := w.WriteTo(bu)
	if err != nil {
		return n, err
	}

	if int(n) >= nc.config.MaxPayload {
		return n, wire.ErrToBig
	}

	nc.network.cu.Lock()
	defer nc.network.cu.Unlock()

	atomic.AddInt64(&nc.totalReadMsgs, 1)
	atomic.AddInt64(&nc.totalWritten, int64(bu.Len()))

	for _, client := range nc.network.clients {
		go func() {
			if _, err := client.writeWithHeader(bu.Bytes()); err != nil {
				nc.logs.WithFields(history.Attrs{
					"from-client": nc.id,
					"to-client":   client.id,
					"data":        string(bu.Bytes()),
				}).Error(err, "failed to broadcast data")
			}
		}()
	}

	return n, nil
}

func (nc *tcpServerClient) writeDirectly(d ...[]byte) error {
	if len(d) == 0 {
		return nil
	}

	if err := nc.isLive(); err != nil {
		return err
	}

	var conn net.Conn
	nc.mu.Lock()
	if nc.conn == nil {
		nc.mu.Unlock()
		return wire.ErrAlreadyClosed
	}
	conn = nc.conn
	nc.mu.Unlock()

	nc.bu.Lock()
	defer nc.bu.Unlock()

	if nc.buffWriter == nil {
		return wire.ErrAlreadyClosed
	}

	for _, item := range d {
		available := nc.buffWriter.Available()

		// if what is available is less than what we have
		// available, then flush first.
		toWrite := len(item) + wire.HeaderLength
		if available < toWrite {
			buffered := nc.buffWriter.Buffered()
			atomic.AddInt64(&nc.totalFlushOut, int64(buffered))

			conn.SetWriteDeadline(time.Now().Add(wire.MaxFlushDeadline))
			if err := nc.buffWriter.Flush(); err != nil {
				conn.SetWriteDeadline(time.Time{})
				return err
			}
			conn.SetWriteDeadline(time.Time{})
		}

		if _, err := nc.buffWriter.Write(item); err != nil {
			return err
		}
	}

	return nil
}

func (nc *tcpServerClient) distribute(skipSelf bool, skipCluster bool, flush bool, data []byte) error {
	nc.network.cu.Lock()
	defer nc.network.cu.Unlock()

	var err error
	for _, client := range nc.network.clients {
		// If skipself is true, then skip this instance from
		// write.
		if skipSelf && (client.id == nc.id || client == nc) {
			continue
		}

		// If skipCluster is true and we are a cluster, then skip this
		// instance from write. We do this because attempting to wire
		// to all cluster here, will calls a endless propagation, has
		// other clusters will be sending back to the source. Its important
		// to only ever ensure if a cluster is sent to then it's done by the
		// source of the message, allowing other clusters to just distribute
		// to their internal network client connections.
		if skipCluster && client.isCluster() {
			continue
		}

		if _, err = client.writeWithHeader(data); err == nil {
			nc.logs.WithFields(history.Attrs{
				"from-client": nc.id,
				"to-client":   client.id,
				"data":        string(data),
			}).Error(err, "failed to broadcast data")
		}

		// If flush is set, then flush all existing data
		// into connection.
		if flush {
			if err := client.flush(); err != nil {
				nc.logs.WithFields(history.Attrs{
					"from-client": nc.id,
					"to-client":   client.id,
					"data":        string(data),
				}).Error(err, "failed to flush broadcasted data")
			}
		}
	}

	return nil
}

// read reads from incoming message handling necessary
// handshake response and requests received over the wire,
// while returning non-hanshake messages.
// It returns pending data if if connection is closed to allow
// user process received data.
func (nc *tcpServerClient) read() ([]byte, error) {
	if cerr := nc.isLive(); cerr != nil {
		return nil, cerr
	}

	return nc.readNext()
}

func (nc *tcpServerClient) readNext() ([]byte, error) {
	indata, err := nc.parser.Next()
	if err != nil {
		return nil, err
	}

	atomic.AddInt64(&nc.totalReadMsgs, 1)
	atomic.AddInt64(&nc.totalRead, int64(len(indata)))

	// We are on the server, if we are not a cluster connection
	// then return to reader else distribute to our own non-cluster
	// connections.
	if bytes.HasPrefix(indata, broadcastBytes) {
		if !nc.isCluster() {
			// Send to all network clients including clusters.
			nc.distribute(true, false, true, indata)

			return bytes.TrimPrefix(indata, broadcastBytes), nil
		}

		// Send to all network clients excluding clusters.
		nc.distribute(true, true, true, indata)

		return nil, wire.ErrNoDataYet
	}

	if bytes.HasPrefix(indata, clientHandshakeCompletedBytes) {
		return nil, wire.ErrNoDataYet
	}

	if bytes.HasPrefix(indata, cinfoBytes) {
		if err := nc.handleCINFO(); err != nil {
			return nil, err
		}
		return nil, wire.ErrNoDataYet
	}

	if bytes.HasPrefix(indata, clStatusBytes) {
		if err := nc.handleCLStatusReceive(indata); err != nil {
			return nil, err
		}
		return nil, wire.ErrNoDataYet
	}

	return indata, nil
}

func (nc *tcpServerClient) getInfo() wire.Info {
	var base wire.Info
	if nc.isCluster() && nc.srcInfo != nil {
		base = *nc.srcInfo
	} else {
		base = wire.Info{
			ID:         nc.id,
			ServerNode: true,
			Cluster:    nc.isACluster,
			MaxPayload: int64(nc.config.MaxPayload),
			ServerAddr: nc.serverAddr.String(),
			Meta:       nc.network.config.Meta,
		}
	}

	if others, err := nc.network.getOtherClients(nc.id); err != nil {
		for _, other := range others {
			if !other.IsCluster() {
				continue
			}
			base.ClusterNodes = append(base.ClusterNodes, other.Info())
		}
	}

	return base
}

func (nc *tcpServerClient) handleCLStatusReceive(data []byte) error {
	data = bytes.TrimPrefix(data, clStatusBytes)

	var info wire.Info
	if err := json.Unmarshal(data, &info); err != nil {
		return err
	}

	nc.srcInfo = &info
	nc.network.registerCluster(nc.serverAddr, info.ClusterNodes)

	bu := guardPool.Get().(*bytes.Buffer)
	bu.Reset()
	defer guardPool.Put(bu)

	bu.Write(handshakeCompletedBytes)
	_, err := nc.writeSize(bu.Len(), bu)
	if err != nil {
		return err
	}

	return nc.flush()
}

func (nc *tcpServerClient) handleCLStatusSend() error {
	var info wire.Info
	info.Cluster = true
	info.ServerNode = true
	info.ID = nc.network.id
	info.Meta = nc.network.config.Meta
	info.MaxPayload = int64(nc.config.MaxPayload)
	info.ServerAddr = nc.serverAddr.String()

	if others, err := nc.network.getOtherClients(nc.id); err != nil {
		for _, other := range others {
			if !other.IsCluster() {
				continue
			}
			info.ClusterNodes = append(info.ClusterNodes, other.Info())
		}
	}

	jsn, err := json.Marshal(info)
	if err != nil {
		return err
	}

	bu := guardPool.Get().(*bytes.Buffer)
	bu.Reset()
	defer guardPool.Put(bu)

	bu.Write(clStatusBytes)
	bu.Write(jsn)

	_, err = nc.writeSize(bu.Len(), bu)
	if err != nil {
		return err
	}

	return nc.flush()
}

func (nc *tcpServerClient) handleCINFO() error {
	jsn, err := json.Marshal(nc.getInfo())
	if err != nil {
		return err
	}

	bu := guardPool.Get().(*bytes.Buffer)
	bu.Reset()

	defer guardPool.Put(bu)

	bu.Write(rinfoBytes)
	bu.Write(jsn)

	_, err = nc.writeSize(bu.Len(), bu)
	if err != nil {
		return err
	}

	return nc.flush()
}

func (nc *tcpServerClient) handleRINFO(data []byte) error {
	data = bytes.TrimPrefix(data, rinfoBytes)

	var info wire.Info
	if err := json.Unmarshal(data, &info); err != nil {
		return err
	}

	info.Cluster = nc.isACluster
	nc.srcInfo = &info

	nc.network.registerCluster(nc.serverAddr, info.ClusterNodes)
	return nil
}

func (nc *tcpServerClient) sendCLHSCompleted() error {
	bu := guardPool.Get().(*bytes.Buffer)
	bu.Reset()
	defer guardPool.Put(bu)

	bu.Write(clientHandshakeCompletedBytes)

	_, err := nc.writeSize(bu.Len(), bu)
	if err != nil {
		return err
	}

	return nc.flush()
}

func (nc *tcpServerClient) sendCINFOReq() error {
	_, err := nc.writeWithHeader(cinfoBytes)
	if err != nil {
		return err
	}

	return nc.flush()
}

func (nc *tcpServerClient) handshake() error {
	if err := nc.sendCINFOReq(); err != nil {
		nc.logs.Error(err, "error sending CINFO request")
		nc.logs.Red("Failed to send CINFO request")
		return err
	}

	before := time.Now()

	// Wait for RCINFO response from connection.
	for {
		msg, err := nc.read()
		if err != nil {
			if err != wire.ErrNoDataYet {
				return err
			}

			if time.Now().Sub(before) > wire.MaxInfoWait {
				nc.closeConn()
				nc.logs.Error(wire.ErrFailedToRecieveInfo, "read timeout")
				nc.logs.Red("read timeout")
				return wire.ErrFailedToRecieveInfo
			}
			continue
		}

		// if we are to skip handshake and this is not a cluster then skip.
		if bytes.Equal(msg, handshakeSkipBytes) && !nc.isACluster {
			nc.logs.Info("handshake process skipped")
			return nil
		}

		// if we get a rescue signal, then client never got our CINFO request, so resent.
		if bytes.Equal(msg, rescueBytes) {
			nc.logs.Info("received rescue request")
			if err := nc.sendCINFOReq(); err != nil {
				nc.logs.Error(err, "failed to send CINFO after rescue request")
				nc.logs.Red("send error with CINFO after rescue")
				return err
			}

			// reset time used for tracking timeout.
			before = time.Now()
			continue
		}

		// First message should be a wire.RCINFO response.
		if !bytes.HasPrefix(msg, rinfoBytes) {
			nc.closeConn()
			nc.logs.Error(wire.ErrFailedToRecieveInfo, "failed to receive RCINFO response")
			nc.logs.Red("failed handshake at RCINFO response")
			return wire.ErrFailedToRecieveInfo
		}

		if err := nc.handleRINFO(msg); err != nil {
			nc.logs.Error(err, "failed to process RCINFO response")
			nc.logs.Red("failed handshake at RCINFO response process")
			return err
		}

		break
	}

	// if its a cluster send Cluster Status message.
	if nc.isACluster {
		nc.logs.Info("client cluster handshake agreement processing")
		if err := nc.handleCLStatusSend(); err != nil {
			nc.logs.Error(err, "failed to send CLStatus message")
			nc.logs.Red("failed handshake at CLStatus message")
			return err
		}

		before = time.Now()

		// Wait for handshake completion signal.
		for {
			msg, err := nc.read()
			if err != nil {
				if err != wire.ErrNoDataYet {
					return err
				}

				if time.Now().Sub(before) > wire.MaxInfoWait {
					nc.closeConn()
					nc.logs.Error(wire.ErrFailedToCompleteHandshake, "response timeout")
					nc.logs.Red("failed to complete handshake")
					return wire.ErrFailedToCompleteHandshake
				}
				continue
			}

			// if we get a rescue signal, then client never got our CLStatus response, so resend.
			if bytes.Equal(msg, rescueBytes) {
				if err := nc.handleCLStatusSend(); err != nil {
					nc.logs.Error(err, "failed to send CLStatus response message")
					nc.logs.Red("failed to complete handshake at CLStatus")
					return err
				}

				// reset time used for tracking timeout.
				before = time.Now()
				continue
			}

			if !bytes.Equal(msg, handshakeCompletedBytes) {
				nc.logs.Error(wire.ErrFailedToCompleteHandshake, "failed to received handshake completion")
				nc.logs.Red("failed to complete handshake at completion signal")
				return wire.ErrFailedToCompleteHandshake
			}

			break
		}
	} else {
		if err := nc.sendCLHSCompleted(); err != nil {
			nc.logs.Error(err, "failed to deliver CLHS handshake completion signal")
			nc.logs.Red("failed non-cluster handshake completion")
			return err
		}
	}

	nc.logs.Info("handshake completed")
	return nil
}

func (nc *tcpServerClient) closeConnection() error {
	if err := nc.isLive(); err != nil {
		nc.logs.Error(err, "client already closed")
		return err
	}

	atomic.StoreInt64(&nc.closed, 1)

	nc.flush()

	nc.mu.Lock()
	if nc.conn == nil {
		nc.logs.Yellow("client already closed")
		return wire.ErrAlreadyClosed
	}
	err := nc.conn.Close()
	nc.mu.Unlock()

	nc.worker.Wait()

	nc.mu.Lock()
	nc.conn = nil
	nc.mu.Unlock()

	nc.bu.Lock()
	nc.buffWriter = nil
	nc.bu.Unlock()

	nc.logs.Info("client closed")
	return err
}

func (nc *tcpServerClient) getRemoteAddr() (net.Addr, error) {
	return nc.remoteAddr, nil
}

func (nc *tcpServerClient) getLocalAddr() (net.Addr, error) {
	return nc.localAddr, nil
}

func (nc *tcpServerClient) closeConn() error {
	return nc.closeConnection()
}

// readLoop handles the necessary operation of reading data from the
// underline connection.
func (nc *tcpServerClient) readLoop() {
	defer nc.closeConnection()
	defer nc.worker.Done()

	var icoBtrs []byte
	for {
		frame, err := nc.reader.ReadHeader()
		if err != nil {
			nc.logs.Error(err, "read header error")
			return
		}

		if frame > nc.config.MaxPayload {
			nc.logs.Error(err, "exceeded allowed payload size")
			return
		}

		inco := make([]byte, frame)
		_, err = nc.reader.Read(icoBtrs)
		if err != nil {
			nc.logs.Error(err, "read error")
			return
		}

		atomic.AddInt64(&nc.totalRead, int64(frame))

		nc.logs.WithFields(history.Attrs{
			"read":  string(inco),
			"total": frame,
		}).Info("received data")

		// Turn slice into pointer, so we can ensure to avoid
		// heap allocation.
		incoPtr := unsafe.Pointer(&inco)
		if err := nc.parser.ParseWithPtr(incoPtr); err != nil {
			nc.logs.Error(err, "parser data error")
			return
		}
	}
}

//************************************************************************
//  TCP Server Implementation
//************************************************************************

// Config provides properties for initiating tcp network.
type Config struct {
	// ServerName sets the desired name to be used for the server.
	ServerName string

	// Handshake provides implementation for processing
	// connections tls handshake.
	Handshake wire.TLSHandshake

	// Handler provides the handler to be called to service clients.
	Handler wire.ConnHandler

	// Hook provides a means to get hook into the lifecycle-processes of
	// the network and client connection and disconnection.
	Hook wire.Hook

	// Dialer to be used to create net.Conn to connect to cluster address
	// in Network.AddCluster. Set to use a custom dialer.
	Dialer *net.Dialer

	// Meta contains user defined gacts for this server which will be send along
	// the transfer. Always ensure not to keep large objects or info in here. It
	// is expected to be small.
	Meta wire.Meta

	// MaxConnection defines the total allowed connections for
	// giving network.
	MaxConnections int

	// MaxClusterRetries specifies allowed max retries used by the
	// RetryPolicy for reconnecting to a disconnected cluster network.
	MaxClusterRetries int

	// ClusterRetryDelay defines the duration used to expand for
	// each retry of reconnecting to a cluster address.
	ClusterRetryDelay time.Duration

	// MaxDataWaitBeforeSlowness defines the duration used to waitt for
	// the processing of a incoming data which after expiration would be
	// seen to be a dreadfully slow client, ensure to give value as appropriate
	// to what is giving to Config.MaximumMessageBufferForClient.
	MaxDataWaitBeforeSlowness time.Duration

	// MaximumMessageBufferForClient sets the maximum message space
	// allowed to for server connection to buffer incoming messages by.
	// This information is sent to the client, along with client data
	// relating to available space left. This way client can actively
	// throttle or speed up message delivery as needed.
	MaximumMessageBufferForClient int

	// MaxBufferSize sets given max size of buffer for client, each client writes collected
	// till flush must not exceed else will not be buffered and will be written directly.
	MaxBufferSize int

	// MaxPayload sets given maximum value of payload size the server will accept
	// else closing connection.
	MaxPayload int
}

func (c *Config) init() error {
	if c.Handler == nil {
		return errors.New("Config.Handler is required")
	}

	if c.Handshake == nil {
		c.Handshake = wire.NoUpgrade{}
	}

	if c.Dialer == nil {
		c.Dialer = &net.Dialer{Timeout: wire.DefaultDialTimeout}
	}

	if c.MaximumMessageBufferForClient <= 0 {
		c.MaximumMessageBufferForClient = wire.MaxMessageBufferInClient
	}

	if c.MaxDataWaitBeforeSlowness <= 0 {
		c.MaxDataWaitBeforeSlowness = wire.MaxSlownessWait
	}

	if c.MaxBufferSize <= 0 {
		c.MaxBufferSize = wire.MaxBufferSize
	}

	if c.MaxConnections <= 0 {
		c.MaxConnections = wire.MaxConnections
	}

	if c.MaxClusterRetries <= 0 {
		c.MaxClusterRetries = wire.MaxReconnectRetries
	}

	if c.ClusterRetryDelay <= 0 {
		c.ClusterRetryDelay = wire.DefaultClusterRetryDelay
	}

	if c.MaxPayload <= 0 {
		c.MaxPayload = wire.MaxPayloadSize
	}

	return nil
}

// Network defines a network which runs on top of provided wire.ConnHandler.
type Network struct {
	id           string
	totalClients int64
	totalClosed  int64
	totalActive  int64
	totalOpened  int64

	waiter  sync.WaitGroup
	config  Config
	logs    history.Ctx
	cu      sync.RWMutex
	clients map[string]*tcpServerClient
}

// Server returns a new instance of the Network which implements the
// wire.Clusters and wire.Connections interfaces.
func Server(c Config) (*Network, error) {
	if err := c.init(); err != nil {
		return nil, err
	}

	return &Network{
		config:  c,
		id:      newID(30),
		clients: make(map[string]*tcpServerClient),
	}, nil
}

// Handle handles the provided connection and instantiates it as necessary.
func (n *Network) Handle(addr net.Addr, conn net.Conn, c <-chan struct{}) error {
	serverName := n.config.ServerName
	if serverName == "" {
		serverName, _, _ = net.SplitHostPort(addr.String())
	}

	conn, err := n.config.Handshake.Upgrade(context.Background(), serverName, conn)
	if err != nil {
		return err
	}

	// setup a goroutine to listen for request to close connection.
	go func() {
		<-c
		conn.Close()
	}()

	// if we have maximum allowed connections, then close new conn.
	if int(atomic.LoadInt64(&n.totalOpened)) >= n.config.MaxConnections {
		return conn.Close()
	}

	atomic.AddInt64(&n.totalClients, 1)
	atomic.AddInt64(&n.totalOpened, 1)
	return n.addClient(addr, conn, nil, false)
}

func (n *Network) Wait() {
	n.waiter.Wait()
}

// AddCluster attempts to add new connection to another mnet tcp server.
// It will attempt to dial specified address returning an error if the
// giving address failed or was not able to meet the handshake protocols.
func (n *Network) AddCluster(serverAddr net.Addr, addr string) error {
	if n.connectedToMeByAddr(addr) {
		return wire.ErrAlreadyServiced
	}

	var err error
	var conn net.Conn

	conn, err = n.config.Dialer.Dial("tcp", addr)
	if err != nil {
		return err
	}

	conn, err = n.config.Handshake.Upgrade(context.Background(), n.config.ServerName, conn)
	if err != nil {
		return err
	}

	if n.connectedToMeByAddr(conn.RemoteAddr().String()) {
		conn.Close()
		return wire.ErrAlreadyServiced
	}

	policy := wire.FunctionPolicy(n.config.MaxClusterRetries, func() error {
		return n.AddCluster(serverAddr, addr)
	}, wire.ExponentialDelay(n.config.ClusterRetryDelay))

	if err := n.addClient(serverAddr, conn, policy, true); err != nil {
		conn.Close()
		return err
	}

	return nil
}

func (n *Network) registerCluster(serverAddr net.Addr, clusters []wire.Info) {
	for _, cluster := range clusters {
		if err := n.AddCluster(serverAddr, cluster.ServerAddr); err != nil {
			n.logs.With("info", cluster).Error(err, "failed to add cluster address")
		}
	}
}

// Statistics returns statics associated with Network.
func (n *Network) Statistics() wire.NetworkStatistic {
	var stats wire.NetworkStatistic
	stats.ID = n.id
	stats.TotalClients = atomic.LoadInt64(&n.totalClients)
	stats.TotalClosed = atomic.LoadInt64(&n.totalClosed)
	stats.TotalOpened = atomic.LoadInt64(&n.totalOpened)
	return stats
}

func (n *Network) getOtherClients(cid string) ([]wire.Client, error) {
	n.cu.Lock()
	defer n.cu.Unlock()

	var clients []wire.Client
	for id, conn := range n.clients {
		if id == cid {
			continue
		}

		var client wire.Client
		client.NID = n.id
		client.ID = conn.id
		client.FlushFunc = conn.flush
		client.LiveFunc = conn.isLive
		client.WriteToFunc = conn.write
		client.WriteFunc = conn.writeWithHeader
		client.InfoFunc = conn.getInfo
		client.BufferedFunc = conn.buffered
		client.BroadCastFunc = conn.broadcast
		client.IsClusterFunc = conn.isCluster
		client.HasPendingFunc = conn.hasPending
		client.StatisticFunc = conn.getStatistics
		client.RemoteAddrFunc = conn.getRemoteAddr
		client.LocalAddrFunc = conn.getLocalAddr
		client.SiblingsFunc = func() ([]wire.Client, error) {
			return n.getOtherClients(cid)
		}

		clients = append(clients, client)
	}

	return clients, nil
}

// connectedToMeByAddr returns true/false if we are already connected to server.
func (n *Network) connectedToMeByAddr(addr string) bool {
	n.cu.Lock()
	defer n.cu.Unlock()

	for _, conn := range n.clients {
		if conn.srcInfo == nil {
			continue
		}

		if conn.srcInfo.ServerAddr == addr {
			return true
		}
	}

	return false
}

// connectedToMe returns true/false if we are already connected to server.
func (n *Network) connectedToMe(conn net.Conn) bool {
	n.cu.Lock()
	defer n.cu.Unlock()

	remote := conn.RemoteAddr().String()
	for _, conn := range n.clients {
		if conn.srcInfo == nil {
			continue
		}

		if conn.srcInfo.ServerAddr == remote {
			return true
		}
	}

	return false
}

func (n *Network) addClient(serverAddr net.Addr, conn net.Conn, policy wire.RetryPolicy, isCluster bool) error {
	defer atomic.AddInt64(&n.totalOpened, -1)
	defer atomic.AddInt64(&n.totalClosed, 1)

	id := newID(30)

	client := wire.Client{
		ID:  id,
		NID: n.id,
	}

	nc := new(tcpServerClient)
	nc.id = id
	nc.nid = n.id
	nc.conn = conn
	nc.network = n
	nc.config = n.config
	nc.isACluster = isCluster
	nc.serverAddr = serverAddr
	nc.localAddr = conn.LocalAddr()
	nc.remoteAddr = conn.RemoteAddr()
	nc.sos = newGuardedBuffer(512)
	nc.parser = new(internal.TaggedMessages)

	// buffered reader and writer
	nc.buffWriter = bufio.NewWriterSize(conn, n.config.MaxBufferSize)
	nc.reader = llio.NewLengthRecvReader(bufio.NewReaderSize(conn, nc.config.MaxBufferSize))

	nc.logs = history.WithFields(map[string]interface{}{
		"client-id":   nc.id,
		"server-id":   nc.nid,
		"server-addr": serverAddr,
		"localAddr":   conn.LocalAddr(),
		"remoteAddr":  conn.RemoteAddr(),
	})

	client.LiveFunc = nc.isLive
	client.InfoFunc = nc.getInfo
	client.ReaderFunc = nc.read
	client.WriteToFunc = nc.write
	client.WriteFunc = nc.writeWithHeader
	client.FlushFunc = nc.flush
	client.CloseFunc = nc.closeConn
	client.BufferedFunc = nc.buffered
	client.IsClusterFunc = nc.isCluster
	client.BroadCastFunc = nc.broadcast
	client.HasPendingFunc = nc.hasPending
	client.LocalAddrFunc = nc.getLocalAddr
	client.StatisticFunc = nc.getStatistics
	client.RemoteAddrFunc = nc.getRemoteAddr
	client.SiblingsFunc = func() ([]wire.Client, error) {
		return n.getOtherClients(nc.id)
	}

	nc.worker.Add(1)
	go nc.readLoop()

	if err := nc.handshake(); err != nil {
		return err
	}

	n.cu.Lock()
	n.clients[id] = nc
	n.cu.Unlock()

	n.waiter.Add(1)
	atomic.AddInt64(&n.totalClients, 1)
	go func() {
		defer n.waiter.Done()

		if err := n.config.Handler(client); err != nil {
			nc.logs.Error(err, "Client connection handler failed")
			nc.closeConnection()
		}

		n.cu.Lock()
		delete(n.clients, id)
		n.cu.Unlock()

		if n.config.Hook != nil {
			if isCluster {
				n.config.Hook.ClusterDisconnected(client)
			} else {
				n.config.Hook.NodeDisconnected(client)
			}
		}

		if policy != nil {
			go func() {
				if err := policy.Retry(); err != nil {
					n.logs.Error(err, "Client retry failed")
				}
			}()
		}
	}()

	if n.config.Hook != nil {
		if isCluster {
			n.config.Hook.ClusterAdded(client)
		} else {
			n.config.Hook.NodeAdded(client)
		}
	}

	return nil
}

type guardedBuffer struct {
	ml sync.Mutex
	bu *bytes.Buffer
}

func newGuardedBuffer(size int) *guardedBuffer {
	return &guardedBuffer{
		bu: bytes.NewBuffer(make([]byte, 0, size)),
	}
}

func (gb *guardedBuffer) Do(b func(*bytes.Buffer)) {
	gb.ml.Lock()
	defer gb.ml.Unlock()
	b(gb.bu)
}

var alphanums = []rune("bcdfghjklmnpqrstvwxz0123456789")

func newID(length int) string {
	b := make([]rune, length)
	for i := range b {
		b[i] = alphanums[rand.Intn(len(alphanums))]
	}
	return string(b)
}
