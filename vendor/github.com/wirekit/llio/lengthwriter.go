package llio

import (
	"bytes"
	"encoding/binary"
	"io"
	"sync"

	"github.com/influx6/faux/pools/pbytes"
)

var (
	//bit4Pool = pbytes.NewBytePool(5000, 4)
	bit4Pool = sync.Pool{New: func() interface{} {
		return make([]byte, 4)
	}}
	buffpool = pbytes.NewBytesPool(128, 30)
)

//**********************************************************************
// DynamicLengthWriter
//**********************************************************************

// DynamicLengthWriter implements a io.WriteCloser which prepends/prefixes
// the writing size of data into the provided writer before written
// the data itself.
type DynamicLengthWriter struct {
	n    int
	ty   int
	area []byte
	w    io.Writer
	buff *bytes.Buffer
}

// NewDynamicLengthWriter returns a new instance of a DynamicLengthWriter which
// pre-prends the length of written data into a giving sized byte slice.
// The initial length provided requests an initial size of the underlying buffer
// to be used.
// Where size represents the ff:
// int16(where size is 2), int32(where size is 4), int64(where size is 8) is used.
func NewDynamicLengthWriter(w io.Writer, initialLength int) *DynamicLengthWriter {
	var area []byte
	area = bit4Pool.Get().([]byte)

	buff := buffpool.Get(initialLength)
	buff.Truncate(0)

	return &DynamicLengthWriter{
		w:    w,
		area: area,
		buff: buff,
	}
}

// Reset resets io.Writer, size and data length to be used by writer for its operations.
func (lw *DynamicLengthWriter) Reset(w io.Writer, initial int) {
	buff := buffpool.Get(initial)
	buff.Truncate(0)

	lw.n = 0
	lw.w = w
	lw.buff = buff

	lw.area = bit4Pool.Get().([]byte)
}

// Close closes this writer and flushes data into underline writer.
func (lw *DynamicLengthWriter) Close() error {
	if lw.n == 0 {
		bit4Pool.Put(lw.area)

		lw.w = nil
		lw.buff = nil
		lw.area = nil
		return nil
	}

	binary.BigEndian.PutUint32(lw.area, uint32(lw.n))

	sizeW, err := lw.w.Write(lw.area)
	if err != nil {
		return err
	}

	bit4Pool.Put(lw.area)

	lw.area = nil

	dataW, err := lw.buff.WriteTo(lw.w)
	if err != nil {
		return err
	}

	lw.buff.Reset()
	buffpool.Put(lw.buff)

	lw.w = nil
	lw.buff = nil

	if sizeW+int(dataW) != lw.n+sizeW {
		return io.ErrShortWrite
	}

	return nil
}

// Write will attempt to copy data within provided slice into writers
// size constrained buffer. If data provided is more than available
// space then an ErrLimitExceeded is returned.
func (lw *DynamicLengthWriter) Write(d []byte) (int, error) {
	n, err := lw.buff.Write(d)
	lw.n += n
	return n, err
}

//**********************************************************************
// LengthWriter
//**********************************************************************

// LengthWriter implements a io.WriteCloser which prepends/prefixes
// the writing size of data into the provided writer before written
// the data itself.
type LengthWriter struct {
	n    int
	max  int
	ty   int
	area []byte
	w    io.Writer
	buff *bytes.Buffer
}

// NewLengthWriter returns a new instance of a LengthWriter which
// appends it's dataLength into a giving sized through size byteslice
// which represent either a int16(where size is 2), int32(where size is 4),
// int64(where size is 8) is used.
func NewLengthWriter(w io.Writer, dataLength int) *LengthWriter {
	var area []byte

	area = bit4Pool.Get().([]byte)

	buff := buffpool.Get(dataLength)
	buff.Truncate(0)

	return &LengthWriter{
		w:    w,
		max:  dataLength,
		area: area,
		buff: buff,
	}
}

// Reset resets io.Writer, size and data length to be used by writer for its operations.
func (lw *LengthWriter) Reset(w io.Writer, dataLength int) {
	area := bit4Pool.Get().([]byte)
	buff := buffpool.Get(dataLength)
	buff.Truncate(0)

	lw.n = 0
	lw.w = w
	lw.area = area
	lw.max = dataLength
	lw.buff = buff
}

// Close closes this writer and flushes data into underline writer.
func (lw *LengthWriter) Close() error {
	if lw.n == 0 {
		bit4Pool.Put(lw.area)
		lw.w = nil
		lw.area = nil
		lw.buff = nil
		return nil
	}

	binary.BigEndian.PutUint32(lw.area, uint32(lw.n))

	sizeW, err := lw.w.Write(lw.area)
	if err != nil {
		return err
	}

	bit4Pool.Put(lw.area)

	lw.area = nil

	dataW, err := lw.w.Write(lw.buff.Bytes()[:lw.n])
	if err != nil {
		return err
	}

	lw.buff.Reset()
	buffpool.Put(lw.buff)

	lw.w = nil
	lw.buff = nil

	if sizeW+int(dataW) != lw.max+sizeW {
		return io.ErrShortWrite
	}

	return nil
}

// Write will attempt to copy data within provided slice into writers
// size constrained buffer. If data provided is more than available
// space then an ErrLimitExceeded is returned.
func (lw *LengthWriter) Write(d []byte) (int, error) {
	rem := lw.max - lw.n
	if len(d) > rem {
		return 0, ErrLimitExceeded
	}

	//n := copy(lw.buff[lw.n:lw.max], d)
	n, err := lw.buff.Write(d)
	if err != nil {
		return n, err
	}

	lw.n += n
	return n, nil
}

//**********************************************************************
// ActionLengthWriter
//**********************************************************************

// WriterAction defines a function type to be used when writer gets closed.
type WriterAction func(size []byte, data []byte) error

// ActionLengthWriter implements a io.WriteCloser which prepends/prefixes
// the writing size of data into the provided writer before written
// the data itself.
type ActionLengthWriter struct {
	n    int
	max  int
	ty   int
	area []byte
	buff *bytes.Buffer
	wx   WriterAction
}

// NewActionLengthWriter returns a new instance of a ActionLengthWriter which
// appends it's dataLength into a giving sized through size byte slice
// which represent either a int16(where size is 2), int32(where size is 4),
// int64(where size is 8) is used.
func NewActionLengthWriter(wx WriterAction, dataLength int) *ActionLengthWriter {
	buff := buffpool.Get(dataLength)
	buff.Truncate(0)

	area := bit4Pool.Get().([]byte)

	return &ActionLengthWriter{
		wx:   wx,
		buff: buff,
		max:  dataLength,
		area: area,
	}
}

// Reset resets action function, size and data length to be used by writer for its operations.
func (lw *ActionLengthWriter) Reset(wx WriterAction, dataLength int) {
	buff := buffpool.Get(dataLength)
	buff.Truncate(0)

	lw.n = 0
	lw.wx = wx
	lw.buff = buff
	lw.max = dataLength
	lw.area = make([]byte, 4)
}

// Close closes this writer and flushes data into underline writer.
func (lw *ActionLengthWriter) Close() error {
	if lw.n == 0 {
		bit4Pool.Put(lw.area)

		lw.wx = nil
		lw.area = nil
		lw.buff = nil
		return nil
	}

	binary.BigEndian.PutUint32(lw.area, uint32(lw.n))
	err := lw.wx(lw.area, lw.buff.Bytes()[:lw.n])

	lw.buff.Reset()
	buffpool.Put(lw.buff)
	bit4Pool.Put(lw.area)

	lw.area = nil

	lw.wx = nil
	lw.area = nil
	lw.buff = nil
	return err
}

// Write will attempt to copy data within provided slice into writers
// size constrained buffer. If data provided is more than available
// space then an ErrLimitExceeded is returned.
func (lw *ActionLengthWriter) Write(d []byte) (int, error) {
	rem := lw.max - lw.n
	if len(d) > rem {
		return 0, ErrLimitExceeded
	}

	//n := copy(lw.buff[lw.n:lw.max], d)
	n, _ := lw.buff.Write(d)
	lw.n += n
	return n, nil
}

//**********************************************************************
// GuardedLengthWriter
//**********************************************************************

// GuardedLengthWriter implements a io.WriteCloser which prepends/prefixes
// the writing size of data into the provided writer before written
// the data itself.
type GuardedLengthWriter struct {
	n      int
	max    int
	area   []byte
	buff   *bytes.Buffer
	guard  *sync.Mutex
	writer io.Writer
}

// Close closes this writer and flushes data into underline writer.
func (lw *GuardedLengthWriter) Close() error {
	if lw.n == 0 {
		lw.reset()
		return nil
	}

	binary.BigEndian.PutUint32(lw.area, uint32(lw.n))

	var n int
	var err error

	lw.guard.Lock()
	defer lw.guard.Unlock()

	n, err = lw.writer.Write(lw.area)
	if err != nil {
		lw.reset()
		return err
	}

	if n != len(lw.area) {
		lw.reset()
		return io.ErrShortWrite
	}

	data := lw.buff.Bytes()[:lw.n]
	n, err = lw.writer.Write(data)
	if err != nil {
		lw.reset()
		return err
	}

	if n != len(data) {
		lw.reset()
		return io.ErrShortWrite
	}

	lw.reset()
	return nil
}

// Reset sets the writer to use provided guard and writer as sink.
func (lw *GuardedLengthWriter) Reset(w io.Writer, guard *sync.Mutex, size int) {
	lw.reset()
	lw.max = size
	lw.writer = w
	lw.guard = guard

	lw.area = bit4Pool.Get().([]byte)
	buff := buffpool.Get(size)
	buff.Truncate(0)
	lw.buff = buff
}

func (lw *GuardedLengthWriter) reset() {
	if lw.buff != nil {
		buffpool.Put(lw.buff)
	}

	if lw.area != nil {
		bit4Pool.Put(lw.area)
	}

	lw.area = nil
	lw.guard = nil
	lw.writer = nil
	lw.area = nil
	lw.buff = nil
	lw.max = 0
	lw.n = 0
}

// Write will attempt to copy data within provided slice into writers
// size constrained buffer. If data provided is more than available
// space then an ErrLimitExceeded is returned.
func (lw *GuardedLengthWriter) Write(d []byte) (int, error) {
	rem := lw.max - lw.n
	if len(d) > rem {
		return 0, ErrLimitExceeded
	}

	//n := copy(lw.buff[lw.n:lw.max], d)
	n, _ := lw.buff.Write(d)
	lw.n += n
	return n, nil
}
