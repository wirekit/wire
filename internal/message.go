package internal

import (
	"errors"
	"sync"

	"io"

	"unsafe"

	"github.com/wirekit/wire"
)

const (
	HeaderLength  = 4
	MaxHeaderSize = uint32(4294967295)
)

// errors ...
var (
	ErrTotalExceeded   = errors.New("specified data size exceeded, rejecting write")
	ErrLimitExceeded   = errors.New("writer space exceeded")
	ErrNotStream       = errors.New("data is not a stream transmission")
	ErrStreamDataParts = errors.New("invalid stream data, expected 2 part pieces")
	ErrHeaderLength    = errors.New("invalid header, header length not matching expected")
	ErrInvalidHeader   = errors.New("invalid header data max size: must be either int16, int32, int64 range")
)

var (
	tombPools = sync.Pool{
		New: func() interface{} {
			return new(messageTomb)
		},
	}
)

// messageTomb defines a single node upon a linked list which
// contains it's data and a link to the next messageTomb node.
type messageTomb struct {
	Data []byte
	Prev *messageTomb
}

// TaggedMessages implements a parser which parses incoming messages
// as a series of [MessageSizeLength][Message] joined together, which it
// splits apart into individual message blocks. The parser keeps a series of internal
// linked list which contains already processed message, which has a endless length
// value which allows appending new messages as they arrive.
type TaggedMessages struct {
	mu   sync.RWMutex
	tail *messageTomb
}

// Parse implements the necessary procedure for parsing incoming data and
// appropriately splitting them accordingly to their respective parts.
func (smp *TaggedMessages) Parse(d []byte) error {
	msg := tombPools.Get().(*messageTomb)
	msg.Data = d

	smp.mu.Lock()

	if smp.tail == nil {
		smp.tail = msg
	} else {
		msg.Prev = smp.tail
		smp.tail = msg
	}

	smp.mu.Unlock()

	return nil
}

func (smp *TaggedMessages) ParseWithPtr(ptr unsafe.Pointer) error {
	ico := *(*[]byte)(unsafe.Pointer(ptr))
	msg := tombPools.Get().(*messageTomb)
	msg.Data = ico

	smp.mu.Lock()

	if smp.tail == nil {
		smp.tail = msg
	} else {
		msg.Prev = smp.tail
		smp.tail = msg
	}

	smp.mu.Unlock()

	return nil
}

func (smp *TaggedMessages) ParseWith(size int, r io.Reader) error {
	ico := make([]byte, size)

	n, err := r.Read(ico)
	if err != nil {
		return err
	}

	if n != size {
		return io.ErrShortWrite
	}

	msg := tombPools.Get().(*messageTomb)
	msg.Data = ico

	smp.mu.Lock()

	if smp.tail == nil {
		smp.tail = msg
	} else {
		msg.Prev = smp.tail
		smp.tail = msg
	}

	smp.mu.Unlock()

	return nil
}

// Next returns the next message saved on the parsers linked list.
func (smp *TaggedMessages) Next() ([]byte, error) {
	smp.mu.RLock()
	defer smp.mu.RUnlock()

	if smp.tail == nil {
		return nil, wire.ErrNoDataYet
	}

	head := smp.tail
	smp.tail = head.Prev
	head.Prev = nil

	data := head.Data
	head.Data = nil
	tombPools.Put(head)

	return data, nil
}
