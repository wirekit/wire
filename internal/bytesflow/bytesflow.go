package bytesflow

import (
	"io"
	"net"
	"sync"
	"time"
)

//*******************************************************************
// Flow Strategy
//*******************************************************************

// FlowStrategy defines a strategy int type to represent WriteFlow's
// behaviour in dealing with write streams.
type FlowStrategy int

// constants types of FlowStrategy.
const (
	// BlockUntilFree will ask the WriteFlow to hold/block the
	// thread until the writer is free and ready to deliver more
	// data or a given time/duration and max miss has being passed,
	// whereby the writer flow is closed and stopped as an a slow
	// consumer was met.
	BlockUntilFree FlowStrategy = iota + 1

	// DropOldest will ask the WriteFlow to drop oldest data to
	// make space for new incoming data, it comes at a cost of
	// dropping packets.
	DropOldest

	// DropNewest will ask the WriteFlow to drop incoming data
	// if sink is considered slow and unable to respond to
	// current pressure.
	DropNewest
)

type dropConfig struct {
	MaxBuffer         int
	DropOldest        bool
	MaxWaitBeforeDrop time.Duration
}

type blockConfig struct {
	MaxBuffer          int
	MaxFreeMiss        int
	MaxWaitingDuration time.Duration
}

//*******************************************************************
// WriteFlow
//*******************************************************************

// WriteFlowOption defines a function type that calls
// against a WriteFlow.
type WriteFlowOption func(flow *WriteFlow)

// DropConfig sets the strategy of a FlowWrite to the DropOldest or DropNewest
// based on the dropOldest boolean flag. It sues the buffer as the maximum buffer space
// for incoming writes and the maxNoDropTime duration to maximum duration to allow
// dropping of messages if no other message gets processed with such time, however if
// maxNoDropTime is zero or -1, then we consider we are allowed to continously drop
// messages continuously.
func DropConfig(buffer int, dropOldest bool, maxNoDropTime time.Duration) WriteFlowOption {
	return func(flow *WriteFlow) {
		if dropOldest {
			flow.strategy = DropOldest
		} else {
			flow.strategy = DropNewest
		}

		flow.timer = time.NewTimer(maxNoDropTime)
		flow.drop = &dropConfig{
			MaxBuffer:         buffer,
			DropOldest:        dropOldest,
			MaxWaitBeforeDrop: maxNoDropTime,
		}
	}
}

// DropNewestConfig returns a WriteFlowOption that drop newest write blocks.
// It calls DropConfig underneath.
func DropNewestConfig(buffer int, maxNoDrop time.Duration) WriteFlowOption {
	return DropConfig(buffer, false, maxNoDrop)
}

// DropOldestConfig returns a WriteFlowOption that drop oldest write blocks.
// It calls DropConfig underneath.
func DropOldestConfig(buffer int, maxNoDrop time.Duration) WriteFlowOption {
	return DropConfig(buffer, true, maxNoDrop)
}

// BlockConfig returns a WriteFlowOption that uses the BlockStrategy for WriteFlow
// writing of incoming write blocks.
func BlockConfig(buffer int, maxAttemptFail int, maxBetweenAttempt time.Duration) WriteFlowOption {
	return func(flow *WriteFlow) {
		flow.strategy = BlockUntilFree
		flow.timer = time.NewTimer(maxBetweenAttempt)
		flow.block = &blockConfig{
			MaxBuffer:          buffer,
			MaxFreeMiss:        maxAttemptFail,
			MaxWaitingDuration: maxBetweenAttempt,
		}
	}
}

// WriteFlow implements a flow-controlled writer which consistently
// writes into a provided writer based on the flow stats it receives
// on available space for reception.
type WriteFlow struct {
	queue  chan []byte
	closer chan struct{}
	wc     io.WriteCloser
	wg     sync.WaitGroup

	strategy FlowStrategy
	timer    *time.Timer
	drop     *dropConfig
	block    *blockConfig
}

// NewWriteFlow returns a new instance of the WriteFlow using the provided
// net.Conn.
func NewWriteFlow(wc net.Conn, ops ...WriteFlowOption) *WriteFlow {
	flow := new(WriteFlow)
	for _, op := range ops {
		op(flow)
	}

	flow.queue = make(chan []byte)
	flow.closer = make(chan struct{})

	flow.wg.Add(1)
	go flow.run()
	return flow
}

// Write will attempt to write provided byte into giving
// underline source if space is available, else will queue
// data to be written into it's internal write queue, if
// queue is full and the underline write source is yet
// to take in data within expected/specified period of
// time the source is considered slow and an action
// based on strategy is applied. See FlowStrategy.
func (wf WriteFlow) Write(d []byte) (int, error) {
	var written int
	return written, nil
}

// Close closes the underline write flow.
func (wf WriteFlow) Close() error {
	return nil
}

func (wf WriteFlow) run() {
	defer wf.wg.Done()

	for {
		select {
		case <-wf.closer:
			return
		}
	}
}

func (wf WriteFlow) runAsBlocking() {

}

func (wf WriteFlow) runAsDropping() {

}
