package mtls

import (
	"errors"
	"math/rand"
	"net"

	"fmt"

	"time"

	"bytes"

	"crypto/tls"

	"encoding/json"

	"encoding/base64"

	"crypto/x509"

	"sync"

	"github.com/gokit/history"
	"github.com/wirekit/llio"
	"github.com/wirekit/tlsfs"
	"github.com/wirekit/tlsfs/certificates"
	"github.com/wirekit/tlsfs/tlsp/owned"
	"github.com/wirekit/wire"
)

// package errors values...
var (
	ErrInvalidToken = errors.New("invalid token received")
	ErrNoTokenName  = errors.New("server name not provided")
	ErrSubCAFailure = errors.New("failed to generated sub CA")
)

var (
	defaultUsage = []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth, x509.ExtKeyUsageClientAuth}
)

// const of message headers and bodies.
const (
	maxUniqueIDSize    = 10
	headerDataLength   = 1024        // 1kb
	frameDataLength    = 1024 * 1024 // 1mb
	aurtlsDomain       = "aurtls.cs"
	aurAgentCommonName = "AurAgent Cert Handshake Authority"
	spacer             = " "

	HSVHeader    = "HSV"
	SIGNINHeader = "SIGNIN"
	SHLDCMessage = "SHLDC"
	SHLDOMessage = "SHLDO"
	AUTHMessage  = "AUTH"
	ERRHeader    = "-ERR"
	OKDoneHeader = "+OK"
	OKCSRHeader  = "+OKCSR"
)

// CertResponse is sent when a node successfully receives a new certificate for
// the AurAgent handshake process.
type CertResponse struct {
	Node        string
	Domain      string
	Email       string
	CSR         []byte
	Certificate []byte
	Issuer      []byte
}

//************************************************************************
// TokenSource
//************************************************************************

// TokenSource defines an interface that exposes a method for retrieving
// the necessary token for a giving address (either ip or other data).
type TokenSource interface {
	GetToken(addr string) (string, error)
}

// MapSource returns a TokenSource which retrieves
// said tokens from provided map.
func MapSource(tokens map[string]string) TokenSource {
	return ActionSource(func(addr string) (string, error) {
		if value, ok := tokens[addr]; ok {
			return value, nil
		}
		return "", errors.New("not found")
	})
}

// TokenAction defines a function type which returns a
// string for a given input.
type TokenAction func(string) (string, error)

// ActionSource returns a new TokenSource whoes values
// are retrieved from from a provided function.
func ActionSource(fn TokenAction) TokenSource {
	return actionSource{fn}
}

type actionSource struct {
	action TokenAction
}

// GetToken returns value of giving address if it exists.
func (c actionSource) GetToken(addr string) (string, error) {
	return c.action(addr)
}

//************************************************************************
// Implements Auto TLS Issuer Server Agent
//************************************************************************

// Config contains fields and settings used by AurAgent for it's
// handshake and certificate issuing process.
type Config struct {
	// TokenName sets the address of the used for retrieving
	// server token from the Config.TokenSource.
	TokenName string

	// Tokens represents the source from which a giving address's
	// secret token will be retrieved from.
	Tokens TokenSource

	// Log sets the logger to be used for logging all operation done by the AurAgent instance.
	Log history.Ctx

	// NodeCerts sets the tlsfs certificate filesystem for use to store node issued
	// long-lived certificates. Ensure TLSFS signing time suites the duration of certificate
	// validity.
	NodeCerts tlsfs.TLSFS

	// AuthCerts sets the tlsfs filesystem to be used for storing short-lived issues
	// certificates used for the certificate issuance handshake process. They are short lived
	// and will be deleted periodically once their lease or use has expired. Ensure lease
	// time is very short.
	AuthCerts *owned.CustomFS

	// SubDNS sets the dns values to be attached to all generated sub CA
	// which will be provided to all connections in the basic handshake
	// section of the AurAgent.
	SubDNS []string

	// connCerts sets the tlsfs certificate filesystem for use and store dynamic
	// on request certificates generated during csr connection handling.
	connCerts tlsfs.TLSFS
}

func (c *Config) init() error {
	if c.TokenName == "" {
		return errors.New("Config.TokenName can not be empty")
	}

	if c.Tokens == nil {
		return errors.New("Config.Token can not be empty")
	}

	if c.AuthCerts == nil {
		return errors.New("Config.AuthCerts can not be nil")
	}

	if c.NodeCerts == nil {
		return errors.New("Config.NodeCerts can not be nil")
	}

	if c.Log == nil {
		c.Log = history.WithTags("mtls-agent-server")
	}

	subCerts, err := owned.BasicFS(aurAgentCommonName, time.Hour*2, time.Hour*1)
	if err != nil {
		return err
	}

	c.connCerts = subCerts
	return nil
}

// AurAgent implements a custom acme based on a three way handshake protocol,
// where a desiring remote client would connect to the service by providing doing the
// following:
// (Note all messages will have the line endings, that is  added at the end.)
//
// 1. Connect to autoTLS server using a tls.Config with InsecureSkipVerify set to true.
// 2. Respond to server with unique secure access token upon reception of a AUTH message (i.e AUTH <Server-Token>) with a HSV message response (i.e "HSV"), if the server-token returned by the server is valid, has the client is required to ensure it has a way to validate that the token received is the expected token for the server, this allows the client to verify if it's talking to the right server.
// 3. Expect a short-lived signed secure tls sub-CA certificate with unique server name to be used for secure reconnection with the SHLDC message. i.e "SHLDC <unique client id> <sub-ca-bytes>". Server expects user to provide user signed temporary certificate with RootCA set to the short-lived CA certificate. User must respond after reception of SHLDC with a SHLDO containing client short-lived CA that server can trust. ie. SHLDO <short-lived-ca-cert>.
// 4. Reconnect with a `SIGNIN <unique client id>` message as response to AUTH message, which after receiving an OK, client must attempt to turn connection into a tls/ssl connection using a tls.Config that has the received short-lived tls sub-CA certificate as trusted RootCA before its expiration, which was received at step 2, else if expired must restart negotiation process at step 1.
// 5. If short-lived certificate is still valid, then connection will not be killed and then client must first turn connection into a tls connection as describe previously, then deliver a +OKCSR message with a CSR as a PEM encoded base64 encoded byte slice, which will be used to create a suitable certificate. An -Err is returned if byte slice is not a valid CSR, where client must reconnect and following from step 4.
// 6. If successful, you will receive a +OK message with long-live tls certificate and bundled CA certificate attached, capable of connecting to any service signed and powered by CA issued certificates. If failure, you will receive a -ERR message.
//
// This steps allow us to provide a fairly secure and capable means of providing self-signed and
// managed certificates to connections, encapsulating certificate turn-over and validation easily.
type AurAgent struct {
	config  Config
	ml      sync.RWMutex
	members map[string]*x509.Certificate
}

// NewAurTLS returns a new instance of AurAgent.
func NewAurTLS(config Config) (*AurAgent, error) {
	if err := config.init(); err != nil {
		return nil, err
	}

	return &AurAgent{
		config:  config,
		members: map[string]*x509.Certificate{},
	}, nil
}

// Handle connection receives the initial tls connection which it queries for
// any initial handshake which it then generates a short-lived 2hr certificate which
// grants the connection limited secure access for which a longer-lived certificate
// and CA bundle will be prepared for connection, on its handshake reconnection
// behaviour.
func (aus *AurAgent) Handle(conn net.Conn) error {
	logc := aus.config.Log.WithFields(history.Attrs{
		"remote-addr": conn.RemoteAddr(),
		"local-addr":  conn.LocalAddr(),
		"agent-name":  aus.config.TokenName,
	}).Info("Handling new connection for Aur handshake")

	reader := llio.NewLengthRecvReader(conn, 4)
	writer := llio.NewDynamicLengthWriter(conn, 4, headerDataLength)
	defer reader.Reset(nil)
	defer writer.Reset(nil, 0)

	// Retrieve token for this giving agent server using config's TokenName.
	serverToken, err := aus.config.Tokens.GetToken(aus.config.TokenName)
	if err != nil {
		// Send -ERR message to connection as response to invalid token.
		writer.Reset(conn, headerDataLength)
		writer.Write([]byte(ERRHeader))

		conn.SetWriteDeadline(time.Now().Add(time.Second * 3))
		if err := writer.Close(); err != nil {
			logc.Error(err, "failed to send error message")
		}
		conn.SetWriteDeadline(time.Time{})
		conn.Close()

		logc.Error(err, "token not found")
		return err
	}

	logc = logc.WithFields(history.Attrs{"agent-token": serverToken})

	logc.With("agent-token", serverToken).Info("Using token for agent server")

	// Send AUTH message to connection and await response.
	// Set write deadline for connection because we don't want to
	// endless wait.
	conn.SetWriteDeadline(time.Now().Add(time.Second * 3))

	writer.Write([]byte(AUTHMessage))
	writer.Write([]byte(spacer))
	writer.Write([]byte(serverToken))
	logc.Info("Sending AUTH message to new client")
	if err := writer.Close(); err != nil {
		logc.Error(err, "failed to send AUTH message")
		conn.SetWriteDeadline(time.Time{})
		conn.Close()
		return err
	}
	conn.SetWriteDeadline(time.Time{})

	// A response must be received within 5 secs else you loose,
	// go restart.
	conn.SetReadDeadline(time.Now().Add(time.Second * 5))
	logc.Info("reading response header from new client")
	header, err := reader.ReadHeader()
	if err != nil {
		logc.Error(err, "failed to reading AUTH response from new client")
		conn.SetReadDeadline(time.Time{})
		conn.Close()
		return err
	}

	logc.Info("reading AUTH response from new client")
	req := make([]byte, header)
	n, err := reader.Read(req)
	if err != nil {
		logc.Error(err, "failed to reading AUTH response from new client")
		conn.SetReadDeadline(time.Time{})
		conn.Close()
		return err
	}
	conn.SetReadDeadline(time.Time{})

	logc = logc.WithFields(history.Attrs{
		"data": string(req),
	})

	logc.With("read", n).Info("received message from new client")

	// if we are dealing with the initial negotiation face of signature authentication
	// and short-lived certificate generation, then go-ahead.
	if bytes.HasPrefix(req, []byte(HSVHeader)) {
		logc.Info("handling HSV handshake")

		if err := aus.handleBasicSignatureAuthorization(logc, conn, reader, writer); err != nil {
			logc.Error(err, "handling HSV handshake")
			return err
		}

		return nil
	}

	// if we are dealing with the second negotiation face of certificate request signing
	// over short-lived securely signed certificate generation, then go-ahead.
	if bytes.HasPrefix(req, []byte(SIGNINHeader)) {
		logc.Info("handling SIGNIN handshake")

		cid := bytes.TrimSpace(bytes.TrimPrefix(req, []byte(SIGNINHeader)))
		if err := aus.handleSignInRequest(logc, string(cid), conn, reader, writer); err != nil {
			logc.Error(err, "handling SIGNIN handshake")
			return err
		}

		return nil
	}

	// Send -ERR message to connection as response to unknown
	// request
	logc.Red("received unknown message header")
	conn.SetWriteDeadline(time.Now().Add(time.Second * 3))
	writer.Write([]byte(ERRHeader))
	writer.Close()
	conn.SetWriteDeadline(time.Time{})
	conn.Close()

	return errors.New("unknown message request")
}

func (aus *AurAgent) handleBasicSignatureAuthorization(log history.Ctx, conn net.Conn, reader *llio.LengthRecvReader, writer *llio.DynamicLengthWriter) error {
	log.Info("connection initial handshake begin")

	// generate unique id for client and unique
	// domain/server name for client to use in creating
	// temporal certificate for secure communication.
	uniqueID := newID(maxUniqueIDSize)
	uniqueDomain := fmt.Sprintf("%s.%s", uniqueID, aurtlsDomain)
	uniqueEmail := fmt.Sprintf("%s@%s", uniqueID, aurtlsDomain)

	log = log.WithFields(history.Attrs{
		"server-agent-server-name":  aus.config.TokenName,
		"server-agent-client-id":    uniqueID,
		"server-agent-client-email": uniqueEmail,
		"server-agent-domain":       uniqueDomain,
	}).Info("upgrade agent member initial communication to tls")

	// create temporary tls connection but use short lived certificate
	// for communication.
	tlsConn, err := wire.ServerConnToTLS(&tls.Config{
		ServerName:     aurtlsDomain,
		GetCertificate: aus.config.connCerts.GetCertificate(uniqueEmail),
	}, conn)
	if err != nil {
		log.Error(err, "failed to upgrade connection tls")

		// Send -ERR message to connection as response to invalid token.
		writer.Reset(conn, headerDataLength)
		writer.Write([]byte(ERRHeader))

		conn.SetWriteDeadline(time.Now().Add(time.Second * 3))
		if err := writer.Close(); err != nil {
			log.Error(err, "failed to send error message")
		}
		conn.SetWriteDeadline(time.Time{})
		conn.Close()
		return err
	}

	reader.Reset(tlsConn)
	writer.Reset(tlsConn, headerDataLength)

	var acct tlsfs.NewDomain
	acct.Version = 1
	acct.Local = "LG"
	acct.Country = "NG"
	acct.Email = uniqueEmail
	acct.Domain = uniqueDomain
	acct.KeyType = tlsfs.ECKey384
	acct.CommonName = "UNIQUE SUBCA Domain"
	acct.DNSNames = append(acct.DNSNames, aus.config.SubDNS...)
	acct.DNSNames = append(acct.DNSNames, uniqueDomain)

	// create short-lived sub-ca for certificate.
	subCA, status, err := aus.config.AuthCerts.CreateCA(acct, tlsfs.AgreeToTOS)
	if err != nil {
		log.Error(err, "failed to create short lived Sub CA for client")

		// Send -ERR message to connection as response to invalid token.
		writer.Reset(tlsConn, headerDataLength)
		writer.Write([]byte(ERRHeader))

		tlsConn.SetWriteDeadline(time.Now().Add(time.Second * 3))
		if err := writer.Close(); err != nil {
			log.Error(err, "failed to send error message")
		}
		tlsConn.SetWriteDeadline(time.Time{})
		tlsConn.Close()
		return err
	}

	if status.Flag() != tlsfs.Created {
		err := errors.New("failed to create sub CA for domain " + uniqueDomain)
		log.Error(err, "failed to create short lived Sub CA for client")

		// Send -ERR message to connection as response to invalid token.
		writer.Reset(tlsConn, headerDataLength)
		writer.Write([]byte(ERRHeader))

		tlsConn.SetWriteDeadline(time.Now().Add(time.Second * 3))
		if err := writer.Close(); err != nil {
			log.Error(err, "failed to send error message")
		}
		tlsConn.SetWriteDeadline(time.Time{})
		tlsConn.Close()
		return err
	}

	encodedCA, err := certificates.EncodeCertificate(subCA.Certificate)
	if err != nil {
		// Send -ERR message to connection as response to invalid token.
		writer.Reset(tlsConn, headerDataLength)
		writer.Write([]byte(ERRHeader))

		tlsConn.SetWriteDeadline(time.Now().Add(time.Second * 3))
		if err := writer.Close(); err != nil {
			log.Error(err, "failed to send error message")
		}
		tlsConn.SetWriteDeadline(time.Time{})
		tlsConn.Close()
		return err
	}

	writer.Reset(tlsConn, frameDataLength)
	if _, err := fmt.Fprintf(writer, "%s %s ", SHLDCMessage, uniqueID); err != nil {
		// Send -ERR message to connection as response to invalid token.
		writer.Reset(tlsConn, headerDataLength)
		writer.Write([]byte(ERRHeader))

		tlsConn.SetWriteDeadline(time.Now().Add(time.Second * 3))
		if err := writer.Close(); err != nil {
			log.Error(err, "failed to send error message")
		}
		tlsConn.SetWriteDeadline(time.Time{})
		tlsConn.Close()
		return err
	}

	b64EncodedCA := base64.StdEncoding.EncodeToString(encodedCA)
	writer.Write([]byte(b64EncodedCA))

	log.With("encoded-ca", b64EncodedCA).Info("encoding sub CA has base64")

	tlsConn.SetWriteDeadline(time.Now().Add(time.Second * 5))
	if err := writer.Close(); err != nil {
		tlsConn.SetWriteDeadline(time.Time{})
		tlsConn.Close()
		log.Error(err, "failed to send encoding CA to client")
		return err
	}
	tlsConn.SetWriteDeadline(time.Time{})

	payHeader, err := reader.ReadHeader()
	if err != nil {
		// Send -ERR message to connection as response to invalid token.
		writer.Reset(tlsConn, headerDataLength)
		writer.Write([]byte(ERRHeader))

		tlsConn.SetWriteDeadline(time.Now().Add(time.Second * 3))
		if err := writer.Close(); err != nil {
			log.Error(err, "failed to send error message")
		}
		tlsConn.SetWriteDeadline(time.Time{})
		tlsConn.Close()
		return err
	}

	payload := make([]byte, payHeader)
	_, err = reader.Read(payload)
	if err != nil {
		// Send -ERR message to connection as response to invalid token.
		writer.Reset(tlsConn, headerDataLength)
		writer.Write([]byte(ERRHeader))

		tlsConn.SetWriteDeadline(time.Now().Add(time.Second * 3))
		if err := writer.Close(); err != nil {
			log.Error(err, "failed to send error message")
		}
		tlsConn.SetWriteDeadline(time.Time{})
		tlsConn.Close()
		return err
	}

	log.With("payload", string(payload)).Info("received SHLDC payload from agent server")

	if !bytes.HasPrefix(payload, []byte(SHLDOMessage)) {
		// Send -ERR message to connection as response to invalid token.
		writer.Reset(tlsConn, headerDataLength)
		writer.Write([]byte(ERRHeader))

		tlsConn.SetWriteDeadline(time.Now().Add(time.Second * 3))
		if err := writer.Close(); err != nil {
			log.Error(err, "failed to send error message")
		}
		tlsConn.SetWriteDeadline(time.Time{})
		tlsConn.Close()
		return err
	}

	payload = bytes.TrimSpace(bytes.TrimPrefix(payload, []byte(SHLDOMessage)))
	decodedMemberCA, err := base64.StdEncoding.DecodeString(string(payload))
	if err != nil {
		// Send -ERR message to connection as response to invalid token.
		writer.Reset(tlsConn, headerDataLength)
		writer.Write([]byte(ERRHeader))

		tlsConn.SetWriteDeadline(time.Now().Add(time.Second * 3))
		if err := writer.Close(); err != nil {
			log.Error(err, "failed to send error message")
		}
		tlsConn.SetWriteDeadline(time.Time{})
		tlsConn.Close()
		return err
	}

	memberCA, err := certificates.DecodeCertificate(decodedMemberCA)
	if err != nil {
		// Send -ERR message to connection as response to invalid token.
		writer.Reset(tlsConn, headerDataLength)
		writer.Write([]byte(ERRHeader))

		tlsConn.SetWriteDeadline(time.Now().Add(time.Second * 3))
		if err := writer.Close(); err != nil {
			log.Error(err, "failed to send error message")
		}
		tlsConn.SetWriteDeadline(time.Time{})
		tlsConn.Close()
		return err
	}

	aus.ml.Lock()
	aus.members[uniqueID] = memberCA
	aus.ml.Unlock()

	log.Info("connection initial handshake completed")
	return nil
}

func (aus *AurAgent) handleSignInRequest(log history.Ctx, id string, conn net.Conn, reader *llio.LengthRecvReader, writer *llio.DynamicLengthWriter) error {
	log.Info("begin sign-in handshake")

	clientEmail := fmt.Sprintf("%s@%s", id, aurtlsDomain)
	clientDomain := fmt.Sprintf("%s.%s", id, aurtlsDomain)

	log = log.WithFields(history.Attrs{
		"client-id":     id,
		"client-email":  clientEmail,
		"client-domain": clientDomain,
	})

	defer func() {
		aus.ml.Lock()
		defer aus.ml.Unlock()
		delete(aus.members, id)
	}()

	// Retrieve member CA certificate immediately.
	var ok bool
	var memberCA *x509.Certificate
	aus.ml.RLock()
	if memberCA, ok = aus.members[id]; !ok {
		aus.ml.RUnlock()
		err := errors.New("bad agent member")
		log.Error(err, "failed to negotiated member CA certificate")

		// Send -ERR message to connection as response to invalid token.
		writer.Reset(conn, headerDataLength)
		writer.Write([]byte(ERRHeader))

		conn.SetWriteDeadline(time.Now().Add(time.Second * 3))
		if err := writer.Close(); err != nil {
			log.Error(err, "failed to send error message")
		}
		conn.SetWriteDeadline(time.Time{})
		conn.Close()
		return err
	}
	aus.ml.RUnlock()

	// Retrieve user's short lived certificate and validate id and lease.
	log.Info("retrieving SUB CA certificate")
	subCA, shStatus, err := aus.config.AuthCerts.Get(clientEmail, clientDomain)
	if err != nil {
		log.Error(err, "failed to get sub CA certificate")

		// Send -ERR message to connection as response to invalid token.
		writer.Reset(conn, headerDataLength)
		writer.Write([]byte(ERRHeader))

		conn.SetWriteDeadline(time.Now().Add(time.Second * 3))
		if err := writer.Close(); err != nil {
			log.Error(err, "failed to send error message")
		}
		conn.SetWriteDeadline(time.Time{})
		conn.Close()
		return err
	}

	log.Info("validating sub ca lease availability")
	if shStatus.Flag() != tlsfs.Live {
		// Send -ERR message to connection as response to invalid token.
		writer.Reset(conn, headerDataLength)
		writer.Write([]byte(ERRHeader))

		conn.SetWriteDeadline(time.Now().Add(time.Second * 3))
		if err := writer.Close(); err != nil {
			log.Error(err, "failed to send error message")
		}
		conn.SetWriteDeadline(time.Time{})
		conn.Close()
		return err
	}

	log.Info("retrieving SUB CA credentials")
	subUser, err := aus.config.AuthCerts.GetUser(clientEmail)
	if err != nil {
		log.Error(err, "failed to get sub CA credentials and private key")

		// Send -ERR message to connection as response to invalid token.
		writer.Reset(conn, headerDataLength)
		writer.Write([]byte(ERRHeader))

		conn.SetWriteDeadline(time.Now().Add(time.Second * 3))
		if err := writer.Close(); err != nil {
			log.Error(err, "failed to send error message")
		}
		conn.SetWriteDeadline(time.Time{})
		conn.Close()
		return err
	}

	fromCA, err := owned.FromCA(subCA.Certificate, subUser.GetPrivateKey(), time.Minute*20)
	if err != nil {
		log.Error(err, "failed to generate subCA host for short lived cert")

		// Send -ERR message to connection as response to invalid token.
		writer.Reset(conn, headerDataLength)
		writer.Write([]byte(ERRHeader))

		conn.SetWriteDeadline(time.Now().Add(time.Second * 3))
		if err := writer.Close(); err != nil {
			log.Error(err, "failed to send error message")
		}
		conn.SetWriteDeadline(time.Time{})
		conn.Close()
		return err
	}

	// Create a cert pool that will only recognize the subCA for this
	// given client.
	pool := x509.NewCertPool()
	pool.AddCert(memberCA)
	pool.AddCert(subCA.Certificate)

	// Attempt to turn connection into tls connection using configured
	// tls.Config that auto-generates short lived certificates for quick
	// use with client.
	tlsConn, err := wire.ServerConnToTLS(&tls.Config{
		ClientCAs:      pool,
		MinVersion:     tls.VersionTLS12,
		ClientAuth:     tls.RequireAndVerifyClientCert,
		GetCertificate: fromCA.GetCertificate(clientEmail),
	}, conn)
	if err != nil {
		log.Error(err, "failed to upgrade client connection to tls connection")

		// Send -ERR message to connection as response to invalid token.
		writer.Reset(conn, headerDataLength)
		writer.Write([]byte(ERRHeader))

		conn.SetWriteDeadline(time.Now().Add(time.Second * 3))
		if err := writer.Close(); err != nil {
			log.Error(err, "failed to send error message")
		}
		conn.SetWriteDeadline(time.Time{})
		conn.Close()
		return err
	}

	reader.Reset(tlsConn)
	writer.Reset(tlsConn, headerDataLength)

	log.Info("connection upgrade successfully")

	tlsConn.SetReadDeadline(time.Now().Add(time.Second * 5))
	header, err := reader.ReadHeader()
	if err != nil {
		log.Error(err, "failed to read header from client")

		// Send -ERR message to connection as response to invalid token.
		writer.Reset(tlsConn, headerDataLength)
		writer.Write([]byte(ERRHeader))

		tlsConn.SetWriteDeadline(time.Now().Add(time.Second * 3))
		if err := writer.Close(); err != nil {
			log.Error(err, "failed to send error message")
		}
		tlsConn.SetWriteDeadline(time.Time{})
		tlsConn.Close()
		return err
	}

	log.With("expected-data-size", header).Info("will be reading data size")

	// Read PEM encoded CSR from client and validate the domain name and
	// and life time matches expected criteria.
	msg := make([]byte, header)
	n, err := reader.Read(msg)
	log.With("received-data-size", n).Info("read data size")
	if err != nil {
		log.Error(err, "failed to read message from client")

		// Send -ERR message to connection as response to invalid token.
		writer.Reset(tlsConn, headerDataLength)
		writer.Write([]byte(ERRHeader))

		tlsConn.SetWriteDeadline(time.Now().Add(time.Second * 3))
		if err := writer.Close(); err != nil {
			log.Error(err, "failed to send error message")
		}
		tlsConn.SetWriteDeadline(time.Time{})
		tlsConn.Close()
		return err
	}
	tlsConn.SetReadDeadline(time.Time{})

	csrByte := msg[:n]
	if !bytes.HasPrefix(csrByte, []byte(OKCSRHeader)) {
		log.Error(err, "failed to match message with OKCSR Header")

		// Send -ERR message to connection as response to invalid token.
		writer.Reset(tlsConn, headerDataLength)
		writer.Write([]byte(ERRHeader))

		tlsConn.SetWriteDeadline(time.Now().Add(time.Second * 3))
		if err := writer.Close(); err != nil {
			log.Error(err, "failed to send error message")
		}
		tlsConn.SetWriteDeadline(time.Time{})
		tlsConn.Close()

		log.Error(err, "failed to received OKCSR message")
		return err
	}

	log.With("csr", string(csrByte)).Info("received response from agent server")
	csrByte = bytes.TrimSpace(bytes.TrimPrefix(csrByte, []byte(OKCSRHeader)))
	log.With("received-CSR", string(csrByte)).Info("received response from agent server")

	csrDecodedBytes, err := base64.StdEncoding.DecodeString(string(csrByte))
	if err != nil {
		log.Error(err, "failed to decode message for CSR response")

		// Send -ERR message to connection as response to invalid token.
		writer.Reset(tlsConn, headerDataLength)
		writer.Write([]byte(ERRHeader))

		tlsConn.SetWriteDeadline(time.Now().Add(time.Second * 3))
		if err := writer.Close(); err != nil {
			log.Error(err, "failed to send error message")
		}
		tlsConn.SetWriteDeadline(time.Time{})
		tlsConn.Close()
		log.Error(err, "failed to decode CSR message")
		return err
	}

	csr, err := certificates.DecodeCertificateRequest(csrDecodedBytes)
	if err != nil {
		log.Error(err, "failed to get CSR from decoded payload")

		// Send -ERR message to connection as response to invalid token.
		writer.Reset(tlsConn, headerDataLength)
		writer.Write([]byte(ERRHeader))

		tlsConn.SetWriteDeadline(time.Now().Add(time.Second * 3))
		if err := writer.Close(); err != nil {
			log.Error(err, "failed to send error message")
		}
		tlsConn.SetWriteDeadline(time.Time{})
		tlsConn.Close()
		log.Error(err, "failed to decode CSR message")
		return err
	}

	newSignedCA, newStatus, err := aus.config.NodeCerts.CreateWithCSR(*csr, tlsfs.AgreeToTOS)
	if err != nil {
		log.Error(err, "failed create certificate from CSR")

		// Send -ERR message to connection as response to invalid token.
		writer.Reset(tlsConn, headerDataLength)
		writer.Write([]byte(ERRHeader))

		tlsConn.SetWriteDeadline(time.Now().Add(time.Second * 3))
		if err := writer.Close(); err != nil {
			log.Error(err, "failed to send error message")
		}
		tlsConn.SetWriteDeadline(time.Time{})
		tlsConn.Close()
		log.Error(err, "failed to create CSR message")
		return err
	}

	if newStatus.Flag() != tlsfs.Renewed && newStatus.Flag() != tlsfs.Live && newStatus.Flag() != tlsfs.Created {
		log.Error(err, "failed certificate generation with invalid status")

		// Send -ERR message to connection as response to invalid token.
		writer.Reset(tlsConn, headerDataLength)
		writer.Write([]byte(ERRHeader))

		tlsConn.SetWriteDeadline(time.Now().Add(time.Second * 3))
		if err := writer.Close(); err != nil {
			log.Error(err, "failed to send error message")
		}
		tlsConn.SetWriteDeadline(time.Time{})
		tlsConn.Close()
		return ErrSubCAFailure
	}

	// encode certificate as pem encoded block.
	certPem, err := certificates.EncodeCertificate(newSignedCA.Certificate)
	if err != nil {
		log.Error(err, "failed encode new signed certificate")

		// Send -ERR message to connection as response to invalid token.
		writer.Reset(tlsConn, headerDataLength)
		writer.Write([]byte(ERRHeader))

		tlsConn.SetWriteDeadline(time.Now().Add(time.Second * 3))
		if err := writer.Close(); err != nil {
			log.Error(err, "failed to send error message")
		}
		tlsConn.SetWriteDeadline(time.Time{})
		tlsConn.Close()
		return err
	}

	// encode issuer certificate as pem encoded block.
	issuerPem, err := certificates.EncodeCertificate(newSignedCA.IssuerCertificate)
	if err != nil {
		log.Error(err, "failed encode new signed certificate's issuer certificate")

		// Send -ERR message to connection as response to invalid token.
		writer.Reset(tlsConn, headerDataLength)
		writer.Write([]byte(ERRHeader))

		tlsConn.SetWriteDeadline(time.Now().Add(time.Second * 3))
		if err := writer.Close(); err != nil {
			log.Error(err, "failed to send error message")
		}
		tlsConn.SetWriteDeadline(time.Time{})
		tlsConn.Close()
		return err
	}

	// Encode new certificate as json with all CSR, Certificate and RootCA as PEM
	// encoded byte slice.
	var response CertResponse
	response.Node = id
	response.CSR = csrByte
	response.Issuer = issuerPem
	response.Certificate = certPem
	response.Email = newSignedCA.User
	response.Domain = newSignedCA.Domain

	responseJSON, err := json.Marshal(response)
	if err != nil {
		log.Error(err, "failed turn response into JSON")

		// Send -ERR message to connection as response to invalid token.
		writer.Reset(tlsConn, headerDataLength)
		writer.Write([]byte(ERRHeader))

		tlsConn.SetWriteDeadline(time.Now().Add(time.Second * 3))
		if err := writer.Close(); err != nil {
			log.Error(err, "failed to send error message")
		}
		tlsConn.SetWriteDeadline(time.Time{})
		tlsConn.Close()
		return err
	}

	writer.Reset(tlsConn, frameDataLength)
	if _, err := fmt.Fprintf(writer, "%s ", OKDoneHeader); err != nil {
		log.Error(err, "failed to write response to writer")
		tlsConn.Close()
		return err
	}

	jsonb64Response := []byte(base64.StdEncoding.EncodeToString(responseJSON))
	log.With("csr-response-payload", string(jsonb64Response)).With("csr-response-length", len(jsonb64Response)).Info("created CSR response payload")

	writer.Write(jsonb64Response)

	//tlsConn.SetWriteDeadline(time.Now().Add(time.Second * 8))
	if err = writer.Close(); err != nil {
		log.Error(err, "failed to write response to client")
		tlsConn.SetWriteDeadline(time.Time{})
		tlsConn.Close()
		return err
	}
	//tlsConn.SetWriteDeadline(time.Time{})

	//tlsConn.SetReadDeadline(time.Now().Add(time.Second * 5))
	lastHeader, err := reader.ReadHeader()
	if err != nil {
		log.Error(err, "failed to read header from client")

		// Send -ERR message to connection as response to invalid token.
		writer.Reset(tlsConn, headerDataLength)
		writer.Write([]byte(ERRHeader))

		tlsConn.SetWriteDeadline(time.Now().Add(time.Second * 3))
		if err := writer.Close(); err != nil {
			log.Error(err, "failed to send error message")
		}
		tlsConn.SetWriteDeadline(time.Time{})
		tlsConn.Close()
		return err
	}

	log.With("expected-data-size", lastHeader).Info("will be reading data size")

	// Read PEM encoded CSR from client and validate the domain name and
	// and life time matches expected criteria.

	lastMessage := make([]byte, lastHeader)
	nn, err := reader.Read(lastMessage)
	log.With("received-data-size", nn).Info("will be reading data size")
	if err != nil {
		log.Error(err, "failed to read message from client")

		// Send -ERR message to connection as response to invalid token.
		writer.Reset(tlsConn, headerDataLength)
		writer.Write([]byte(ERRHeader))

		tlsConn.SetWriteDeadline(time.Now().Add(time.Second * 3))
		if err := writer.Close(); err != nil {
			log.Error(err, "failed to send error message")
		}
		tlsConn.SetWriteDeadline(time.Time{})
		tlsConn.Close()
		return err
	}
	//tlsConn.SetReadDeadline(time.Time{})

	if !bytes.Equal(lastMessage, []byte(OKDoneHeader)) {
		log.Error(err, "failed to OK signal")
	}

	return tlsConn.Close()
}

//************************************************************************
// Implements Auto TLS Issuer Client Agent
//************************************************************************

// AurMember implements client handshake process, necessary to
// negotiate for a tls certificate from a Aurfs server agent.
type AurMember struct {
	// Tokens represents the source from which a giving address's
	// secret token will be retrieved from.
	Tokens TokenSource

	// CSR represents the CSR to be sent by the member during
	// during the second phase of authorization.
	CSR *x509.CertificateRequest

	// Dialer provides a net.Dialer to be used in dialing connections
	// to a remote AurAgent server.
	Dialer *net.Dialer

	// serverSubCA sets the sub CA received from the server during the
	// initial handshake.
	sub *x509.Certificate

	// clientID sets the unique server id retrieved from the server.
	clientID string

	// memTLS defines the internal memory ca used to generate
	// very short lived private key and certificate to be used
	// in the negotiation for a certificate.
	memTLS *owned.CustomFS
}

func (au *AurMember) init() error {
	if au.Dialer == nil {
		au.Dialer = &net.Dialer{
			Timeout: 5 * time.Second,
		}
	}

	mem, err := owned.BasicFS("Member CA", time.Hour*20, time.Minute*30)
	if err != nil {
		return err
	}

	au.memTLS = mem

	return nil
}

// Handshake performs the necessary request and procedure to
// request a certificate from AurAgent server.
// Handshake receives the connecting address and tokenName
// used to retrieve secret server token from the token source
// which then is used to verify server connection.
func (au *AurMember) Handshake(addr string, tokenName string) (CertResponse, error) {
	log := history.WithFields(history.Attrs{
		"agent-server": addr,
	})

	if err := au.init(); err != nil {
		log.Error(err, "failed to successfully initialize state")
		return CertResponse{}, err
	}

	if err := au.handleBasicHandshake(log, addr, tokenName); err != nil {
		log.Error(err, "failed to successfully complete basic handshake")
		return CertResponse{}, err
	}

	res, err := au.handleSIGNINHandshake(log, addr, tokenName)
	if err != nil {
		log.Error(err, "failed to successfully complete SIGNIN handshake")
		return CertResponse{}, err
	}

	log.Info("AurMember handshake completed")
	return res, nil
}

func (au *AurMember) handleSIGNINHandshake(log history.Ctx, addr string, tokenName string) (CertResponse, error) {
	domain := fmt.Sprintf("%s.%s", au.clientID, aurtlsDomain)
	email := fmt.Sprintf("%s@%s", au.clientID, aurtlsDomain)

	log = log.WithFields(history.Attrs{
		"client-agent-member-id":     au.clientID,
		"client-agent-member-domain": domain,
		"client-agent-member-email":  email,
	})

	var res CertResponse
	conn, err := au.Dialer.Dial("tcp", addr)
	if err != nil {
		log.Error(err, "Should have successfully dialed AurAgent server")
		return res, err
	}

	defer conn.Close()

	reader := llio.NewLengthRecvReader(conn, 4)
	writer := llio.NewDynamicLengthWriter(conn, 4, headerDataLength)

	defer reader.Reset(nil)
	defer writer.Reset(nil, 0)

	header, err := reader.ReadHeader()
	if err != nil {
		log.Error(err, "failed to read header from client connection")
		return res, err
	}

	log.With("expecting-data-size", header).Info("will be reading data size")

	msg := make([]byte, header)
	nm, err := reader.Read(msg)
	log.With("received-data-size", nm).Info("read data size")
	if err != nil {
		log.Error(err, "failed to read header body from client connection")
		return res, err
	}

	log.With("message", string(msg)).Info("received message from server")

	if !bytes.HasPrefix(msg, []byte(AUTHMessage)) {
		err := errors.New("failed to receive initial message")
		log.Error(err, "failed to read header body from client connection")
		return res, err
	}

	serverToken := bytes.TrimSpace(bytes.TrimPrefix(msg, []byte(AUTHMessage)))
	log = log.With("server-sent-token", string(serverToken))

	token, err := au.Tokens.GetToken(tokenName)
	if err != nil {
		log.Error(err, "failed to get server authentication token for address")
		return res, err
	}

	log = log.With("client-received-token", token)

	if token != string(serverToken) {
		err := errors.New("invalid server token received")
		log.Error(err, "invalid token received from server")
		return res, err
	}

	fmt.Fprintf(writer, "%s %s", SIGNINHeader, au.clientID)
	if err := writer.Close(); err != nil {
		log.Red("failed to write SIGNIN message")
		return res, errors.New("failed to write HSV segment")
	}

	userCert, status, err := au.memTLS.Create(tlsfs.NewDomain{
		Version:    1,
		Email:      email,
		Domain:     domain,
		DNSNames:   []string{domain},
		KeyType:    tlsfs.ECKey384,
		CommonName: "AurMember CA",
	}, tlsfs.AgreeToTOS)
	if err != nil {
		log.Error(err, "failed to write create short lived certificate for signin handshake")
		return res, err
	}

	if status.Flag() != tlsfs.Created && status.Flag() != tlsfs.Live {
		log.Error(err, "failed to create certificate from authority")
		return res, errors.New("invalid certificate status")
	}

	user, err := au.memTLS.GetUser(email)
	if err != nil {
		log.Error(err, "failed to retrieve client tls certificate user")
		return res, err
	}

	myCert, err := certificates.MakeTLSCertificate(userCert.Certificate, user.GetPrivateKey())
	if err != nil {
		log.Error(err, "failed to create client tls certificate")
		return res, err
	}

	pool := x509.NewCertPool()
	pool.AddCert(au.sub)

	config := new(tls.Config)
	config.RootCAs = pool
	config.ServerName = domain
	config.MinVersion = tls.VersionTLS12
	config.Certificates = append(config.Certificates, myCert)
	config.GetClientCertificate = func(info *tls.CertificateRequestInfo) (*tls.Certificate, error) {
		return &myCert, nil
	}

	tlsConn, err := wire.ClientConnToTLS(config, conn)
	if err != nil {
		log.Error(err, "failed to complete client tls handshake")
		return res, err
	}

	reader.Reset(tlsConn)
	writer.Reset(tlsConn, headerDataLength)

	encodedCSR, err := certificates.EncodeCertificateRequest(au.CSR)
	if err != nil {
		log.Error(err, "failed to encode client CSR")
		return res, err
	}

	baseEncodedCSR := base64.StdEncoding.EncodeToString(encodedCSR)

	fmt.Fprintf(writer, "%s %s", OKCSRHeader, baseEncodedCSR)
	if err := writer.Close(); err != nil {
		log.Red("failed to write SIGNIN message")
		return res, errors.New("failed to write HSV segment")
	}

	payHeader, err := reader.ReadHeader()
	if err != nil {
		log.Error(err, "failed to read header from client tls connection")
		return res, err
	}

	log.With("expecting-data-size", payHeader).Info("will be reading data size")
	payload := make([]byte, payHeader)
	pn, err := reader.Read(payload)
	log.With("received-data-size", pn).Info("read data size")
	if err != nil {
		log.Error(err, "failed to read from client tls connection")
		return res, err
	}

	// Send final ok header.
	writer.Reset(tlsConn, headerDataLength)
	writer.Write([]byte(OKDoneHeader))
	if err := writer.Close(); err != nil {
		log.Red("failed to write SIGNIN message")
		return res, errors.New("failed to write HSV segment")
	}

	log.With("payload", string(payload)).Info("received +OK payload from agent server")

	if !bytes.HasPrefix(payload, []byte(OKDoneHeader)) {
		log.Red("failed to receive +OK signal message")
		return res, errors.New("failed to receive initial message")
	}

	payload = bytes.TrimSpace(bytes.TrimPrefix(payload, []byte(OKDoneHeader)))
	log.With("payload", string(payload)).Info("received CSR certificate bundle response")

	decoded, err := base64.StdEncoding.DecodeString(string(payload))
	if err != nil {
		log.Error(err, "failed to decode response for client")
		return res, err
	}

	if err := json.Unmarshal(decoded, &res); err != nil {
		log.Error(err, "failed to decode response JSON from agent server")
		return res, err
	}

	return res, nil
}

func (au *AurMember) handleBasicHandshake(log history.Ctx, addr string, tokenName string) error {
	conn, err := au.Dialer.Dial("tcp", addr)
	if err != nil {
		log.Error(err, "Should have successfully dialed AurAgent server")
		return err
	}

	log = log.With("agent-server-addr", addr)

	defer conn.Close()

	reader := llio.NewLengthRecvReader(conn, 4)
	writer := llio.NewDynamicLengthWriter(conn, 4, headerDataLength)

	header, err := reader.ReadHeader()
	if err != nil {
		log.Error(err, "failed to read header from client connection")
		return err
	}

	msg := make([]byte, header)
	_, err = reader.Read(msg)
	if err != nil {
		log.Error(err, "failed to read header body from client connection")
		return err
	}

	log.With("message", string(msg)).Info("received message from server")

	if !bytes.HasPrefix(msg, []byte(AUTHMessage)) {
		err := errors.New("failed to receive initial message")
		log.Error(err, "failed to read header body from client connection")
		return err
	}

	serverToken := bytes.TrimSpace(bytes.TrimPrefix(msg, []byte(AUTHMessage)))
	log = log.With("server-sent-token", string(serverToken))

	token, err := au.Tokens.GetToken(tokenName)
	if err != nil {
		log.Error(err, "failed to get server authentication token for address")
		return err
	}

	log = log.With("client-received-token", token)

	if token != string(serverToken) {
		err := errors.New("invalid server token received")
		log.Error(err, "invalid token received from server")
		return err
	}

	writer.Write([]byte(HSVHeader))
	if err = writer.Close(); err != nil {
		err2 := errors.New("failed to write HSV segment")
		log.Error(err2, "failed to send HSV message")
		return err2
	}

	log = log.WithFields(history.Attrs{
		"member-server-name": aurtlsDomain,
	}).Info("upgrade member client connection into tls connection with server")

	tlsConn, err := wire.ClientConnToTLS(&tls.Config{
		InsecureSkipVerify: true,
		ServerName:         aurtlsDomain,
		MinVersion:         tls.VersionTLS12,
	}, conn)

	if err != nil {
		log.Error(err, "failed to turn net.Conn into tls.Conn on client side")
		return err
	}

	reader.Reset(tlsConn)
	writer.Reset(tlsConn, headerDataLength)

	payHeader, err := reader.ReadHeader()
	if err != nil {
		log.Error(err, "failed to read header from client tls connection")
		return err
	}

	payload := make([]byte, payHeader)
	_, err = reader.Read(payload)
	if err != nil {
		log.Error(err, "failed to read from client tls connection")
		return err
	}

	log.With("payload", string(payload)).Info("received SHLDC payload from agent server")

	if !bytes.HasPrefix(payload, []byte(SHLDCMessage)) {
		log.Red("failed to receive SHLDC signal message")
		return errors.New("failed to receive SHLDC payload")
	}

	payload = bytes.TrimSpace(bytes.TrimPrefix(payload, []byte(SHLDCMessage)))

	parts := bytes.Split(payload, []byte(spacer))
	if len(parts) != 2 {
		err := errors.New("received invalid SHLDC payload parts")
		log.Error(err, "invalid payload for SHLDC received")
		return err
	}

	au.clientID = string(parts[0])
	decodedCA, err := base64.StdEncoding.DecodeString(string(parts[1]))
	if err != nil {
		log.Error(err, "encoded CA payload was not base64 encoded")
		return err
	}

	subCA, err := certificates.DecodeCertificate(decodedCA)
	if err != nil {
		log.Error(err, "unable to decode certificate for CA")
		return err
	}

	au.sub = subCA

	memberCA := au.memTLS.RootCA()

	encodedMemberCA, err := certificates.EncodeCertificate(memberCA.Certificate)
	if err != nil {
		log.Error(err, "unable to encode member certificate for CA")
		return err
	}

	base64MemberCA := base64.StdEncoding.EncodeToString(encodedMemberCA)

	// Send short-lived short-lived CA used by you to server,
	// server has trust issues.
	writer.Write([]byte(SHLDOMessage))
	writer.Write([]byte(spacer))
	writer.Write([]byte(base64MemberCA))
	if err = writer.Close(); err != nil {
		err2 := errors.New("failed to write HSV segment")
		log.Error(err2, "failed to send HSV message")
		return err2
	}

	log.Info("certificate acquisition completed")
	return nil
}

//*******************************************************************************
// Utilities
//*******************************************************************************

var alphanums = []rune("bcdfghjklmnpqrstvwxz0123456789")

func newID(length int) string {
	b := make([]rune, length)
	for i := range b {
		b[i] = alphanums[rand.Intn(len(alphanums))]
	}
	return string(b)
}
