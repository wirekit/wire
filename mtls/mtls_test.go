package mtls_test

import (
	"testing"

	"context"
	"crypto/elliptic"
	"time"

	"github.com/gokit/history"
	"github.com/gokit/history/handlers/std"
	"github.com/influx6/faux/netutils"
	"github.com/influx6/faux/tests"
	"github.com/wirekit/tlsfs"
	"github.com/wirekit/tlsfs/certificates"
	"github.com/wirekit/tlsfs/tlsp/owned"
	"github.com/wirekit/wire/mtls"
)

var _ = history.SetDefaultHandlers(std.Std)

func TestAurAgentAsListener(t *testing.T) {
	nodes, err := owned.BasicFS("AurAgent Nodes Authority", tlsfs.OneYear, 4*time.Hour)
	if err != nil {
		tests.FailedWithError(err, "Should have successfully created nodes certificates")
	}
	tests.Passed("Should have successfully created nodes certificates")

	auths, err := owned.BasicFS("AurAgent Auth Authority", tlsfs.OneYear, 4*time.Hour)
	if err != nil {
		tests.FailedWithError(err, "Should have successfully created auths certificates")
	}
	tests.Passed("Should have successfully created auths certificates")

	tokens := mtls.ActionSource(func(s string) (string, error) {
		return "32323223-323r23r2r2r-32232", nil
	})

	tokenName := "rackniz.io"

	agent := mtls.NewAgentListener(mtls.Config{
		NodeCerts: nodes,
		AuthCerts: auths,
		Tokens:    tokens,
		TokenName: tokenName,
	})

	addr := netutils.ResolveAddr(":0")

	ctx, cancel := context.WithCancel(context.Background())
	if err := agent.Start(ctx, addr); err != nil {
		tests.FailedWithError(err, "Should have successfully created agent listener")
	}
	tests.Passed("Should have successfully created agent listener")

	req, err := certificates.CreateCertificateRequest(certificates.CertificateRequestProfile{
		Version:    1,
		ECCurve:    elliptic.P384(),
		CommonName: "checkboard.cs",
		DNSNames:   []string{"checkboard.cs"},
		Emails:     []string{"shurage@wumzag.com"},
	})
	if err != nil {
		tests.FailedWithError(err, "Should have successfully created req certificates")
	}
	tests.Passed("Should have successfully created req certificates")

	var member mtls.AurMember
	member.Tokens = tokens
	member.CSR = req.Request

	_, err = member.Handshake(addr, tokenName)
	if err != nil {
		tests.FailedWithError(err, "Should have successfully handshaked for address")
	}
	tests.Passed("Should have successfully handshaked for address")

	cancel()
	agent.Wait()
}
