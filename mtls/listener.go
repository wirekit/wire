package mtls

import (
	"context"
	"net"
	"sync"
	"time"

	"github.com/gokit/history"
	"github.com/wirekit/wire"
)

// AgentListener implements a connection tls manager which uses AurAgent to
// service all incoming connections has tls certificate issuance requests.
type AgentListener struct {
	config     Config
	provider   *AurAgent
	waiter     sync.WaitGroup
	connWaiter sync.WaitGroup
}

// NewAgentListener returns a new instance of the AgentListener which uses the
// AurAgent to service all incoming net connections.
func NewAgentListener(c Config) *AgentListener {
	return &AgentListener{
		config: c,
	}
}

// Wait blocks until the agent listener is closed/stopped by
// the passed in context.
func (al *AgentListener) Wait() {
	al.waiter.Wait()
}

// Start runs the AgentListener to run accordingly on the provided address
// on a TCP listener. It will keep running on the provided context is cancelled
// or closed through it's done channel.
func (al *AgentListener) Start(ctx context.Context, addr string) error {
	log := history.WithFields(history.Attrs{
		"addr": addr,
	})

	listener, err := net.Listen("tcp", addr)
	if err != nil {
		return err
	}

	provider, err := NewAurTLS(al.config)
	if err != nil {
		return err
	}

	al.provider = provider

	al.waiter.Add(2)
	go al.stop(log, ctx, listener)
	go al.run(log, listener)
	return nil
}

// run listeners for incoming connections from the listening
// servicing them using the created AurAgent which gets sent
// into a go-routine to avoid blocking other incoming connections.
func (al *AgentListener) run(log history.Ctx, li net.Listener) {
	defer al.waiter.Done()
	defer log.Info("closed agent listener run loop")

	initial := wire.MinTemporarySleep

	// listen for closure of swarm server and
	// end listener.
	for {
		conn, err := li.Accept()
		if err != nil {
			if ne, ok := err.(*net.OpError); ok {
				if ne.Temporary() {
					if initial >= wire.MaxTemporarySleep {
						return
					}

					time.Sleep(initial)
					initial *= 2
					continue
				}
			}

			log.Error(err, "connection closed through listener")
			li.Close()
			return
		}

		if initial >= wire.MaxTemporarySleep {
			initial = wire.MinTemporarySleep
		}

		log2 := log.WithFields(history.Attrs{
			"conn-local-addr":  conn.LocalAddr(),
			"conn-remote-addr": conn.RemoteAddr(),
		}).Info("new agent member connection")

		al.connWaiter.Add(1)
		go func(lg2 history.Ctx) {
			defer al.connWaiter.Done()

			if err := al.provider.Handle(conn); err != nil {
				lg2.Error(err, "failed to successfully handle connection")
			}
		}(log2)
	}
}

// stop listeners for the closing of the provided context which then closes the
// provided listener and after ensuring all connection go-routines are stopped
// will then reduce the wait group counter.
func (al *AgentListener) stop(log history.Ctx, ctx context.Context, li net.Listener) {
	defer al.waiter.Done()
	<-ctx.Done()

	log.Info("Closing AgentListener")
	if err := li.Close(); err != nil {
		log.Error(err, "agent listener net.Listener closed with error")
	}

	// wait for all connections to be fully closed.
	al.connWaiter.Wait()

	log.Info("closed agent listener")
}
